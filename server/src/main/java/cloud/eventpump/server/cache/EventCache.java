package cloud.eventpump.server.cache;


import cloud.eventpump.common.EventCacheStatistics;
import cloud.eventpump.server.EventAdapter;
import cloud.eventpump.server.EventsProvider;

import java.util.List;

import static cloud.eventpump.common.EventCacheStatistics.HitType.*;


public class EventCache<Event> implements EventsProvider<Event> {

    private final EventsProvider<Event> eventsProvider;
    private final EventAdapter<Event> eventAdapter;
    private final EventCacheStorage<Event> eventCacheStorage;
    private EventCacheStatistics eventCacheStatistics = new EventCacheStatistics() {
    };

    public static <Event> EventCache<Event> create(EventsProvider<Event> eventsProvider, EventAdapter<Event> eventAdapter) {
        return new EventCache<>(eventsProvider, eventAdapter, new EventCacheStorage<>());
    }

    public EventCache(EventsProvider<Event> eventsProvider, EventAdapter<Event> eventAdapter, EventCacheStorage<Event> eventCacheStorage) {
        this.eventsProvider = eventsProvider;
        this.eventAdapter = eventAdapter;
        this.eventCacheStorage = eventCacheStorage;
    }

    public void setStatistics(EventCacheStatistics eventCacheStatistics) {
        this.eventCacheStatistics = eventCacheStatistics;
    }


    @Override
    public List<Event> provideEvents(long dbId, int maxEventCount) {
        List<Event> events = eventCacheStorage.get(dbId);
        if (events != null) {
            appendCacheFollowingElements(events, maxEventCount);
            eventCacheStatistics.apply(EventsFoundInCache);
            return events;
        }
        events = eventsProvider.provideEvents(dbId, maxEventCount);
        if (!events.isEmpty()) {
            eventCacheStatistics.apply(EventsNotFoundInCache);
            eventCacheStorage.put(dbId, events);
            return events;
        }
        eventCacheStatistics.apply(NoEventsProvided);
        return events;
    }

    private void appendCacheFollowingElements(List<Event> events, int maxEventCount) {
        for (; ; ) {
            long followingCacheElementsId = eventAdapter.getEventId(events.get(events.size() - 1));
            List<Event> followingCacheElements = eventCacheStorage.get(followingCacheElementsId);
            if (followingCacheElements == null) {
                return;
            }
            copyUpTpMaxEventCount(events, followingCacheElements, maxEventCount);
            if (events.size() >= maxEventCount) {
                return;
            }
        }
    }

    private void copyUpTpMaxEventCount(List<Event> dest, List<Event> source, int maxEventCount) {
        int toCopy = Math.min(source.size(), maxEventCount - dest.size());
        for (int i = 0; i < toCopy; i++) {
            dest.add(source.get(i));
        }
    }

}
