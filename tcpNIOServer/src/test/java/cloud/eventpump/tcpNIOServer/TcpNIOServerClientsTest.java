package cloud.eventpump.tcpNIOServer;

import cloud.eventpump.common.buffers.PrimitiveConversions;
import cloud.eventpump.common.buffers.ReadMultiBuffer;
import cloud.eventpump.common.buffers.WriteMultiBuffer;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProcessor;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToLong;
import static org.junit.Assert.assertEquals;

public class TcpNIOServerClientsTest implements TcpNIOConnectionProcessor {

    private int closeConnectionCount = 0;
    private int interruptCount = 0;

    private TcpNIOServer server;
    private List<ServerConnection> serverConnections;

    private TcpNIOClient clients[] = new TcpNIOClient[10];
    private List<ClientConnection> clientConnections;

    @Before
    public void before() throws IOException, InterruptedException {
        closeConnectionCount = 0;
        interruptCount = 0 ;

        server = new TcpNIOServer(this, new UnexpectedHandler());
        server.start(new InetSocketAddress(9875));
        serverConnections = new ArrayList<>();

        clientConnections = new ArrayList<>();
        for (int i = 0; i < clients.length; i++) {
            TcpNIOClient client = new TcpNIOClient();
            ClientConnection clientConnection = new ClientConnection();
            TcpNIOConnectionProvides clientProvides = client.init(new InetSocketAddress(9875), null, clientConnection);
            clientConnection.setTcpNIOConnectionProvides(clientProvides);
            Thread thread = new Thread( () -> {
                try {
                    client.mainLoop(0) ;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } ) ;
            thread.start();
            clients[i] = client;
            clientConnections.add(clientConnection);
        }
        Thread.sleep(50);
    }

    @Test
    public void serverClientsSmallFramesTest() throws IOException, InterruptedException {
        assertEquals(clients.length, serverConnections.size());

        int count = 1000;
        for (ServerConnection serverConnection : serverConnections) {
            serverConnection.send(count);
            count += 1000;
        }

        Thread.sleep(50);
        long clientsSum = clientConnections.stream()
                .mapToLong(ClientConnection::getReceivedCounter)
                .sum();
        long serversSum = serverConnections.stream()
                .mapToLong(ServerConnection::getReceivedCounter)
                .sum();
        assertEquals( (1000 + 10000)/2*10, clientsSum);
        assertEquals( (1000 + 10000)/2*10, serversSum);

        for (int i = 0; i < clients.length; i++) {
            clients[i].close();
        }
        server.close();
    }

    @Test
    public void interruptTest() throws IOException, InterruptedException {
        assertEquals(clients.length, serverConnections.size());

        server.sendInterrupt();
        Thread.sleep(10);

        assertEquals(10, interruptCount);

        for (int i = 0; i < clients.length; i++) {
            clients[i].close();
        }
        server.close();
    }

    @Override
    public TcpNIOConnectionRequires createConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        ServerConnection serverConnection = new ServerConnection(tcpNIOConnectionProvides);
        serverConnections.add(serverConnection);
        return serverConnection;
    }

    @Override
    public void closeConnection(TcpNIOConnectionRequires tcpNIOConnectionRequires) {
        closeConnectionCount++;
    }


    class ServerConnection implements TcpNIOConnectionRequires {

        private final TcpNIOConnectionProvides tcpNIOConnectionProvides;
        private final ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        private final WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(10 * 1024);

        private int sentCounter = 1;
        private volatile int receivedCounter = 0;

        public ServerConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
            this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        }

        @Override
        public void received(ByteBuffer byteBuffer) {
            readMultiBuffer.append(byteBuffer);
            while (readMultiBuffer.getSize() >= 8) {
                long receivedValue = bytesToLong(readMultiBuffer.getBytes(8));
                assertEquals(receivedCounter + 1, receivedValue);
                receivedCounter++;
            }
        }

        @Override
        public void interrupted() {
            interruptCount++ ;
        }

        public void send(int count) {
            for (int i = 0; i < count; i++) {
                writeMultiBuffer.putBytes(PrimitiveConversions.longToBytes(sentCounter++));
            }
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }

        public int getReceivedCounter() {
            return receivedCounter;
        }
    }

    class ClientConnection implements TcpNIOConnectionRequires {

        private TcpNIOConnectionProvides tcpNIOConnectionProvides;

        private ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        private WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(10 * 1024);

        private int sentCounter = 1;
        private volatile int receivedCounter = 0;

        public void setTcpNIOConnectionProvides(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
            this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        }

        public int getReceivedCounter() {
            return receivedCounter;
        }

        @Override
        public void received(ByteBuffer byteBuffer) {
            readMultiBuffer.append(byteBuffer);
            while (readMultiBuffer.getSize() >= 8) {
                long receivedValue = bytesToLong(readMultiBuffer.getBytes(8));
                assertEquals(receivedCounter + 1, receivedValue);
                receivedCounter++;
            }
            send( receivedCounter- sentCounter +1);
        }

        @Override
        public void interrupted() {
        }

        public void send(int count) {
            for (int i = 0; i < count; i++) {
                writeMultiBuffer.putBytes(PrimitiveConversions.longToBytes(sentCounter++));
            }
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }
    }
}