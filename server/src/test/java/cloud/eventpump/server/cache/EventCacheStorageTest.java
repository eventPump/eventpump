package cloud.eventpump.server.cache;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class EventCacheStorageTest {

    private EventCacheStorage<Integer> eventCacheStorage;

    @Before
    public void before() {
        eventCacheStorage = new EventCacheStorage<>();
    }

    @Test
    public void retainThreeElementsTest() {
        put(2);
        assertEquals(-1, get(1));
        assertEquals(2, get(2));
        assertEquals(-1, get(3));
        assertEquals(-1, get(4));
        assertEquals(-1, get(5));

        put(3);
        assertEquals(-1, get(1));
        assertEquals(2, get(2));
        assertEquals(3, get(3));
        assertEquals(-1, get(4));
        assertEquals(-1, get(5));

        put(4);
        assertEquals(-1, get(1));
        assertEquals(2, get(2));
        assertEquals(3, get(3));
        assertEquals(4, get(4));
        assertEquals(-1, get(5));

        put(5);
        assertEquals(-1, get(1));
        assertEquals(-1, get(2));
        assertEquals(3, get(3));
        assertEquals(4, get(4));
        assertEquals(5, get(5));
    }


    @Test
    public void LRUTest() {
        put(2);
        put(3);

        assertEquals(2, get(2));

        put(4);
        put(5);

        assertEquals(-1, get(1));
        assertEquals(2, get(2));
        assertEquals(-1, get(3));
        assertEquals(4, get(4));
        assertEquals(5, get(5));
        assertEquals(-1, get(6));

        assertEquals(2, get(2));
        assertEquals(4, get(4));
        put(6);

        assertEquals(-1, get(1));
        assertEquals(2, get(2));
        assertEquals(-1, get(3));
        assertEquals(4, get(4));
        assertEquals(-1, get(5));
        assertEquals(6, get(6));
    }


    private int get(int value) {
        List<Integer> integers = eventCacheStorage.get(value);
        if (integers == null) {
            return -1;
        }
        assertEquals(1, integers.size());
        return integers.get(0);
    }


    private void put(int value) {
        eventCacheStorage.put(value, Collections.singletonList(value));
    }

}