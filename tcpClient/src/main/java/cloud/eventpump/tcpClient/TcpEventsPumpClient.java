package cloud.eventpump.tcpClient;


import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.tcpClient.connection.ConnectionProcessor;
import cloud.eventpump.tcpNIOServer.TcpNIOClient;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;

import java.io.IOException;

public class TcpEventsPumpClient<Event> {

    private final static String TCP_EVENT_PUMP_CLIENT_THREAD_NAME = "TcpTcpEventsPumpClient";

    private final EventsApplier<Event> eventsApplier;
    private final TcpClientConfig<Event> config;

    private TcpNIOClient tcpNIOClient;
    private ConnectionProcessor<Event> connectionProcessor;

    private volatile boolean clientConnected = false;

    public TcpEventsPumpClient(EventsApplier<Event> eventsApplier, TcpClientConfig<Event> config) {
        this.eventsApplier = eventsApplier;
        this.config = config;
    }

    public void start() throws IOException {
        connect();
        Thread thread = new Thread(() -> {
            try {
                clientConnected = true;
                tcpNIOClient.mainLoop(0);
            } catch (Exception ex) {
                config.getUnexpectedHandler().unexpectedExceptionInTcpClientThread(ex);
            } finally {
                clientConnected = false;
            }
        }, TCP_EVENT_PUMP_CLIENT_THREAD_NAME);
        thread.start();
    }

    public void startContinuous() {
        Thread thread = new Thread(() -> {
            try {
                repeatingConnectionLoop();
            } catch (Exception ex) {
                config.getUnexpectedHandler().unexpectedExceptionInTcpClientThread(ex);
            }
        }, TCP_EVENT_PUMP_CLIENT_THREAD_NAME);
        thread.start();
    }

    public void ack(long ack) {
        if (connectionProcessor != null) {
            connectionProcessor.ack(ack);
        } else {
            throw new UnsupportedOperationException("Function can be called only after start function has been called");
        }
    }

    public boolean isClientConnected() {
        return clientConnected;
    }

    public void close() {
        if (tcpNIOClient != null) {
            tcpNIOClient.close();
        }
    }

    private void repeatingConnectionLoop() {
        for (; ; ) {
            if (!safeConnect()) {
                return;
            }
            if (!safeExchangeData()) {
                return;
            }
        }
    }

    private boolean safeConnect() {
        int repeatConnectionSleepTime = config.getRepeatConnectionBasicDelay();
        for (; ; ) {
            try {
                connect();
                return true;
            } catch (Exception ex) {
                if (!config.getUnexpectedHandler().exceptionDuringConnect(ex)) {
                    return false;
                }
            }
            connectRepeatSleep(repeatConnectionSleepTime);
            repeatConnectionSleepTime = Math.min(
                    (int) (repeatConnectionSleepTime * config.getRepeatConnectionMultiplier()),
                    config.getRepeatConnectionMaxDelay());
        }
    }

    private boolean safeExchangeData() {
        try {
            clientConnected = true;
            return tcpNIOClient.mainLoop(config.getKeepAliveFrameDelay());
        } catch (Exception ex) {
            tcpNIOClient.clean();
            return config.getUnexpectedHandler().clientExceptionDuringDataExchange(ex);
        } finally {
            clientConnected = false;
        }
    }

    private void connect() throws IOException {
        long lastAppliedDBId = eventsApplier.findLastAppliedDBId();
        tcpNIOClient = new TcpNIOClient();
        connectionProcessor = new ConnectionProcessor<>(
                eventsApplier,
                config.getEventUnmarshaller(),
                config.getUnexpectedHandler(),
                config.getWriteBufferSize(),
                config.getNoDataFromServerTimeout());
        TcpNIOConnectionProvides tcpNIOConnectionProvides = tcpNIOClient.init(
                config.getServer(),
                config.getLocal(),
                connectionProcessor);
        connectionProcessor.setTcpNIOConnectionProvides(tcpNIOConnectionProvides);
        connectionProcessor.init(lastAppliedDBId, config.getClientName());
        tcpNIOClient.setTimeoutHandler(connectionProcessor::timeoutHandler);
    }

    private void connectRepeatSleep(int repeatConnectionSleepTime) {
        try {
            Thread.sleep(repeatConnectionSleepTime);
        } catch (InterruptedException ex) {
            config.getUnexpectedHandler().sleepInterruptedException(ex);
        }
    }


}
