package cloud.eventpump.tcpClient.connection.unmarshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.marshaller.UnmarshallerHost;
import cloud.eventpump.common.marshaller.EventUnmarshaller;

import java.util.List;

public class ClientUnmarshaller<Event> extends UnmarshallerHost {

    public ClientUnmarshaller(EventUnmarshaller<Event> eventEventUnmarshaller, EventConsumer<Event> eventEventConsumer) {
        registerUnmarshaller(FrameNo.DATA_v0100, new PEventUnmarshaller<>(eventEventUnmarshaller, eventEventConsumer));
        registerUnmarshaller(FrameNo.PONG_v100, new PongUnmarshaller<>());
    }

}
