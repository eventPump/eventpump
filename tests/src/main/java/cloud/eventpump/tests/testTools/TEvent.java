package cloud.eventpump.tests.testTools;


public class TEvent {

    private long dbSequence;
    private String content;
    private long utcTS;

    public TEvent() {
    }

    public TEvent(long dbSequence, String content) {
        this.dbSequence = dbSequence;
        this.content = content;
        this.utcTS = System.currentTimeMillis();
    }

    public long getDbSequence() {
        return dbSequence;
    }

    public String getContent() {
        return content;
    }

    public long getUtcTS() {
        return utcTS;
    }

    @Override
    public String toString() {
        return "TEvent{" +
                "dbSequence=" + dbSequence +
                ", content='" + '\'' + content + '\'' +
                ", utsTS='" + utcTS +
                '}';
    }
}
