package cloud.eventpump.tcpNIOServer;

import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnection;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.function.Supplier;

public class TcpNIOClient {

    private Selector selector;
    private TcpNIOConnection tcpNIOConnection;

    private volatile boolean closePending = false;
    private volatile boolean wokenUpBecauseOfSend = false;
    private Supplier<Boolean> timeoutHandler;

    public TcpNIOConnectionProvides init(SocketAddress server, SocketAddress local, TcpNIOConnectionRequires requires) throws IOException {
        selector = Selector.open();
        SocketChannel socketChannel = SocketChannel.open();
        if (local != null) {
            socketChannel.bind(local);
        }
        socketChannel.connect(server);
        socketChannel.configureBlocking(false);
        tcpNIOConnection = new TcpNIOConnection(socketChannel, this::wakeUpForSend);
        tcpNIOConnection.setTcpNIOConnectionRequires(requires);
        return tcpNIOConnection;
    }

    public void setTimeoutHandler(Supplier<Boolean> timeoutHandler) {
        this.timeoutHandler = timeoutHandler;
    }

    public void clean() {
        try {
            if (selector != null) {
                selector.close();
            }
            tcpNIOConnection.close();
        } catch (Exception e) {
            // intentionally empty
        }
    }

    public boolean mainLoop(int sleep) throws Exception {
        for (; ; ) {
            tcpNIOConnection.register(selector);
            long startSelectSleep = System.currentTimeMillis();
            int selectorCount = selector.select(sleep);
            if (selectorCount == 0) {
                if (closePending) {
                    clean();
                    return false;
                }
                long endSelectSleep = System.currentTimeMillis();
                if (sleep > 0 && endSelectSleep - startSelectSleep + 10 >= sleep) {
                    if (timeoutHandler != null && !timeoutHandler.get()) {
                        clean();
                        return true;
                    }
                }
                if (wokenUpBecauseOfSend) {
                    wokenUpBecauseOfSend = false;
                } else {
//                    logger.warn("Rebuilding selector !!!!");
//                    rebuildSelector();
                }
            } else {
                if (!processSelected()) {
                    clean();
                    return true;
                }
            }
        }
    }

    public void close() {
        closePending = true;
        if (selector != null) {
            selector.wakeup();
        }
    }

    private boolean processSelected() {
        Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
        while (keyIterator.hasNext()) {
            SelectionKey key = keyIterator.next();
            if (key.isReadable()) {
                if (!processRead(key)) {
                    return false;
                }
            }
            if (key.isValid() && key.isWritable()) {
                processWrite(key);
            }
            keyIterator.remove();
        }
        return true;
    }

    private boolean processRead(SelectionKey key) {
        TcpNIOConnection connection = tcpNIOConnectionForKey(key);
        return connection.processReadyToRead();
    }

    private void processWrite(SelectionKey key) {
        TcpNIOConnection connection = tcpNIOConnectionForKey(key);
        connection.processReadyToWrite();
    }

    private TcpNIOConnection tcpNIOConnectionForKey(SelectionKey key) {
        return (TcpNIOConnection) key.attachment();
    }

    private void wakeUpForSend() {
        wokenUpBecauseOfSend = true;
        selector.wakeup();
    }
}
