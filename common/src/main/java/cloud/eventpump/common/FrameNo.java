package cloud.eventpump.common;

public interface FrameNo {

    // client to server
    int INIT_v0100 = 1;
    int ACK_v0100 = 2;
    int PING_v0100 = 3;

    // server to client
    int DATA_v0100 = 1;
    int PONG_v100 = 2;
}
