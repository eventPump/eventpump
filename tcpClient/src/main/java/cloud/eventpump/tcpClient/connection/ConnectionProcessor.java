package cloud.eventpump.tcpClient.connection;


import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.common.marshaller.EventUnmarshaller;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpClient.connection.marshaller.ClientMarshaller;
import cloud.eventpump.tcpClient.connection.unmarshaller.ClientUnmarshaller;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;

import java.nio.ByteBuffer;
import java.util.List;

public class ConnectionProcessor<Event> implements TcpNIOConnectionRequires {

    private final ClientUnmarshaller<Event> clientUnmarshaller;
    private final ClientMarshaller clientMarshaller;
    private final UnexpectedHandler unexpectedHandler;
    private TcpNIOConnectionProvides tcpNIOConnectionProvides;

    private final EventsApplier<Event> eventsApplier;

    private int noDataFromServerTimeout;
    private long lastHeardFromServerTime = -1;

    public ConnectionProcessor(EventsApplier<Event> eventsApplier, EventUnmarshaller<Event> eventEventUnmarshaller,
                               UnexpectedHandler unexpectedHandler, int writeBufferSize, int noDataFromServerTimeout) {
        this.eventsApplier = eventsApplier;
        this.clientUnmarshaller = new ClientUnmarshaller<>(eventEventUnmarshaller, this::processPEvents);
        this.clientMarshaller = new ClientMarshaller(writeBufferSize);
        this.unexpectedHandler = unexpectedHandler;
        this.noDataFromServerTimeout = noDataFromServerTimeout ;
    }

    private void processPEvents(List<Event> pEvents) {
        long ack = eventsApplier.applyEvents(pEvents);
        if (ack > 0) {
            ack(ack);
        }
    }

    public void init(long lastAppliedEvent, String clientName) {
        tcpNIOConnectionProvides.send(clientMarshaller.marshallInit(lastAppliedEvent, clientName));
    }

    public void ack(long ack) {
        tcpNIOConnectionProvides.send(clientMarshaller.marshallAck(ack));
    }

    private void ping() {
        tcpNIOConnectionProvides.send(clientMarshaller.marshallPing());
    }

    @Override
    public void received(ByteBuffer byteBuffer) {
        setLastHeardFromServer();
        clientUnmarshaller.processReadData(byteBuffer);
    }

    private void setLastHeardFromServer() {
        lastHeardFromServerTime = System.currentTimeMillis();
    }

    public void setTcpNIOConnectionProvides(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
    }

    public boolean timeoutHandler() {
        if (timeoutElapsed()) {
            lastHeardFromServerTime = -1 ;
            return false;
        }
        ping();
        return true;
    }

    private boolean timeoutElapsed() {
        if (lastHeardFromServerTime != -1) {
            long lastHeardFromServerDuration = System.currentTimeMillis() - lastHeardFromServerTime;
            return lastHeardFromServerDuration > noDataFromServerTimeout;
        }
        return false;
    }

}
