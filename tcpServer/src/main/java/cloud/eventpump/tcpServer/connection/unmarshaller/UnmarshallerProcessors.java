package cloud.eventpump.tcpServer.connection.unmarshaller;

public interface UnmarshallerProcessors {
    void startDBId(long dbId);

    void clientName(String clientName);

    void ack(long ack);

    void ping() ;
}
