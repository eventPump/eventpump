package cloud.eventpump.tcpServer.connection;


import cloud.eventpump.common.marshaller.EventMarshaller;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.connection.EPSConnectionProvides;
import cloud.eventpump.server.connection.EPSConnectionRequires;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;
import cloud.eventpump.tcpServer.connection.malshaller.ServerMarshaller;
import cloud.eventpump.tcpServer.connection.unmarshaller.ServerUnmarshaller;
import cloud.eventpump.tcpServer.connection.unmarshaller.UnmarshallerProcessors;

import java.nio.ByteBuffer;
import java.util.List;

public class ConnectionProcessor<Event> implements TcpNIOConnectionRequires, EPSConnectionRequires<Event>, UnmarshallerProcessors {

    private final EventMarshaller<Event> eventMarshaller;
    private final UnexpectedHandler unexpectedHandler;
    private int writeBufferSize;

    private EPSConnectionProvides epConnection;
    private TcpNIOConnectionProvides tcpNIOConnectionProvides;
    private ServerUnmarshaller serverUnmarshaller;
    private ServerMarshaller<Event> serverMarshaller;

    public ConnectionProcessor(EventMarshaller<Event> eventMarshaller, int writeBufferSize, UnexpectedHandler unexpectedHandler) {
        this.eventMarshaller = eventMarshaller;
        this.unexpectedHandler = unexpectedHandler;
        this.writeBufferSize = writeBufferSize;
    }

    public void init(EPSConnectionProvides epConnection, TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        this.epConnection = epConnection;
        this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        this.serverUnmarshaller = new ServerUnmarshaller(this);
        this.serverMarshaller = new ServerMarshaller<>(eventMarshaller, writeBufferSize, unexpectedHandler);
    }

    public EPSConnectionProvides getEpConnection() {
        return epConnection;
    }

    @Override // TcpNIOConnectionRequires
    public void received(ByteBuffer byteBuffer) {
        epConnection.applyReceivedBytesCount(byteBuffer.limit());
        serverUnmarshaller.processReadData(byteBuffer);
    }

    @Override // TcpNIOConnectionRequires
    public void interrupted() {
        epConnection.processCommit();
    }

    @Override // EPDataConnectionRequires
    public void processEvents(List<Event> events) {
        send(serverMarshaller.marshallEvents(events));
    }

    @Override //UnmarshallerProcessors
    public void startDBId(long dbId) {
        epConnection.setStartDBId(dbId);
    }

    @Override //UnmarshallerProcessors
    public void clientName(String clientName) {
        epConnection.setClientName(clientName);
    }

    @Override //UnmarshallerProcessors
    public void ack(long ack) {
        epConnection.ack(ack);
    }

    @Override //UnmarshallerProcessors
    public void ping() {
        send(serverMarshaller.marshallPong());
    }

    private void send(List<ByteBuffer> byteBuffers) {
        byteBuffers.forEach(bb -> epConnection.applySentBytesCount(bb.limit()));
        tcpNIOConnectionProvides.send(byteBuffers);
    }

}
