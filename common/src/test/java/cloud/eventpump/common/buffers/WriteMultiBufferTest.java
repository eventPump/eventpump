package cloud.eventpump.common.buffers;

import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class WriteMultiBufferTest {

    @Test
    public void putBytes1Test() {
        WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(3);
        writeMultiBuffer.putBytes("ab".getBytes());
        writeMultiBuffer.putBytes("cd".getBytes());
        writeMultiBuffer.putBytes("efghi".getBytes());

        List<ByteBuffer> byteBuffers = writeMultiBuffer.getByteBuffers();
        assertEquals(3, byteBuffers.size());
        assertEquals("abc", getContentAsString(byteBuffers.get(0)));
        assertEquals("def", getContentAsString(byteBuffers.get(1)));
        assertEquals("ghi", getContentAsString(byteBuffers.get(2)));
    }

    @Test
    public void putBytes2Test() {
        WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(3);
        writeMultiBuffer.putBytes("abcd".getBytes());
        writeMultiBuffer.putBytes("e".getBytes());
        writeMultiBuffer.putBytes("f".getBytes());
        writeMultiBuffer.putBytes("gh".getBytes());

        List<ByteBuffer> byteBuffers = writeMultiBuffer.getByteBuffers();
        assertEquals(3, byteBuffers.size());
        assertEquals("abc", getContentAsString(byteBuffers.get(0)));
        assertEquals("def", getContentAsString(byteBuffers.get(1)));
        assertEquals("gh", getContentAsString(byteBuffers.get(2)));
    }

    @Test
    public void putBytes3Test() {
        WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(3);
        writeMultiBuffer.putBytes("a".getBytes());
        writeMultiBuffer.putBytes("bc".getBytes());
        writeMultiBuffer.putBytes("def".getBytes());
        writeMultiBuffer.putBytes("g".getBytes());

        List<ByteBuffer> byteBuffers = writeMultiBuffer.getByteBuffers();
        assertEquals(3, byteBuffers.size());
        assertEquals("abc", getContentAsString(byteBuffers.get(0)));
        assertEquals("def", getContentAsString(byteBuffers.get(1)));
        assertEquals("g", getContentAsString(byteBuffers.get(2)));
    }

    @Test
    public void putBytes4Test() {
        WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(3);
        writeMultiBuffer.putBytes("abcd".getBytes());

        List<ByteBuffer> byteBuffers = writeMultiBuffer.getByteBuffers();
        assertEquals(2, byteBuffers.size());
        assertEquals("abc", getContentAsString(byteBuffers.get(0)));
        assertEquals("d", getContentAsString(byteBuffers.get(1)));

        writeMultiBuffer.putBytes("ef".getBytes());
        writeMultiBuffer.putBytes("gh".getBytes());
        byteBuffers = writeMultiBuffer.getByteBuffers();
        assertEquals(2, byteBuffers.size());
        assertEquals("efg", getContentAsString(byteBuffers.get(0)));
        assertEquals("h", getContentAsString(byteBuffers.get(1)));
    }

    @Test
    public void intPlaceholderTest() {
        WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(3);
        writeMultiBuffer.putBytes("ab".getBytes());
        WriteMultiBuffer.PlaceholderInfo placeholderInfo = writeMultiBuffer.reservePlaceholder(5);
        writeMultiBuffer.putBytes("def".getBytes());
        writeMultiBuffer.overwritePlaceholder( placeholderInfo, "ABCDE".getBytes());


        List<ByteBuffer> byteBuffers = writeMultiBuffer.getByteBuffers();

        assertEquals("abA", getContentAsString(byteBuffers.get(0)));
        assertEquals("BCD", getContentAsString(byteBuffers.get(1)));
        assertEquals("Ede", getContentAsString(byteBuffers.get(2)));
        assertEquals("f", getContentAsString(byteBuffers.get(3)));
    }



    private String getContentAsString(ByteBuffer byteBuffer) {
        int limit = byteBuffer.limit();
        byte[] result = new byte[limit];
        byteBuffer.get(result);
        return new String(result);
    }

}