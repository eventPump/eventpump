package cloud.eventpump.common.unexpected;

public class CloseAppException extends RuntimeException {
    public CloseAppException() {
    }

    public CloseAppException(String message) {
        super(message);
    }

    public CloseAppException(String message, Throwable cause) {
        super(message, cause);
    }

    public CloseAppException(Throwable cause) {
        super(cause);
    }

    public CloseAppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
