package cloud.eventpump.common.buffers;

public interface PrimitivesReadBuffer {

    int getInt() ;
    long getLong() ;
    String getString() ;
    byte[] getBytes() ;
}
