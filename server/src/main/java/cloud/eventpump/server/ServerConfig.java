package cloud.eventpump.server;

import cloud.eventpump.common.unexpected.UnexpectedHandler;

public class ServerConfig {

    private int maxBufferedEventsCount = 1000;
    private int eventsReadThreshold = maxBufferedEventsCount / 2;

    private UnexpectedHandler unexpectedHandler = new UnexpectedHandler();

    private String serverName = "EventPump Server";

    public ServerConfig() {
    }

    public void setBufferingParameters(int maxBufferedEventsCount, int eventsReadThreshold) {
        checkValueGraterThan("maxBufferedEventsCount", maxBufferedEventsCount, 0);
        checkValueGraterThan("eventsReadThreshold", eventsReadThreshold, 0);
        checkValueGraterThan("maxBufferedEventsCount", maxBufferedEventsCount, eventsReadThreshold);
        this.maxBufferedEventsCount = maxBufferedEventsCount ;
        this.eventsReadThreshold = eventsReadThreshold ;
    }

    public int getMaxBufferedEventsCount() {
        return maxBufferedEventsCount;
    }

    public int getEventsReadThreshold() {
        return eventsReadThreshold;
    }

    public UnexpectedHandler getUnexpectedHandler() {
        return unexpectedHandler;
    }

    public void setUnexpectedHandler(UnexpectedHandler unexpectedHandler) {
        this.unexpectedHandler = unexpectedHandler;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    private void checkValueGraterThan(String valueName, int value, int toCheck) {
        if (value <= toCheck) {
            throw new IllegalArgumentException(valueName + ": " + value + " should be grated than " + toCheck);
        }
    }
}
