package cloud.eventpump.tcpNIOServer.connection;

import java.nio.ByteBuffer;

public interface TcpNIOConnectionRequires {

    void received(ByteBuffer byteBuffer) ;
    default void interrupted() {};

}
