package cloud.eventpump.common;

public interface EventCacheStatistics {

    enum HitType {EventsFoundInCache, EventsNotFoundInCache, NoEventsProvided}

    default void apply(HitType hitType) {
    }

}
