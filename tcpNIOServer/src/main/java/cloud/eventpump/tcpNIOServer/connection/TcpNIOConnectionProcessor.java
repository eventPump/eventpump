package cloud.eventpump.tcpNIOServer.connection;

public interface TcpNIOConnectionProcessor {

    TcpNIOConnectionRequires createConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides);

    void closeConnection(TcpNIOConnectionRequires tcpNIOConnectionRequires) ;

}
