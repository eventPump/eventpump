package cloud.eventpump.server.cache;

import java.util.List;

class EventCacheItem<Event> {

    final long startDBId ;
    final List<Event> events ;

    EventCacheItem(long startDBId, List<Event> events) {
        this.startDBId = startDBId;
        this.events = events;
    }

}
