package cloud.eventpump.server.connection;


import cloud.eventpump.common.UpdateConnectionStatistics;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.EventAdapter;
import cloud.eventpump.server.EventsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class EPSConnectionManager<Event> {

    private final Logger logger = LoggerFactory.getLogger(EPSConnectionManager.class);

    private final EventsProvider<Event> eventsProvider;
    private final EventAdapter<Event> eventAdapter;
    private final UnexpectedHandler unexpectedHandler;

    private final List<EPSConnectionProcessor<Event>> epsConnectionProcessors = new ArrayList<>();

    public EPSConnectionManager(EventsProvider<Event> eventsProvider, EventAdapter<Event> eventAdapter, UnexpectedHandler unexpectedHandler) {
        this.eventsProvider = eventsProvider;
        this.eventAdapter = eventAdapter;
        this.unexpectedHandler = unexpectedHandler;
    }

    public synchronized EPSConnectionProvides createConnection(
            EPSConnectionRequires<Event> epDataConnectionRequires,
            UpdateConnectionStatistics updateConnectionStatistics,
            int eventsReadThreshold, int maxBufferedEventsCount) {
        // create processor
        BufferedEventsInfo bufferedEventsInfo = new BufferedEventsInfo(maxBufferedEventsCount);
        AckProcessor<Event> ackProcessor = new AckProcessor<>(eventAdapter, updateConnectionStatistics, bufferedEventsInfo, unexpectedHandler);
        ReadController<Event> readController = new ReadController<>(eventsProvider, eventsReadThreshold);
        EPSConnectionProcessor<Event> connectionProcessor = new EPSConnectionProcessor<>(
                ackProcessor, readController, epDataConnectionRequires, updateConnectionStatistics);
        epsConnectionProcessors.add(connectionProcessor);
        logger.info("Client connect to server");
        return connectionProcessor;
    }

    public synchronized UpdateConnectionStatistics closeConnection(EPSConnectionProvides epsConnectionProvides) {
        int idx = epsConnectionProcessors.indexOf(epsConnectionProvides);
        if (idx != -1) {
            UpdateConnectionStatistics updateConnectionStatistics = epsConnectionProcessors.get(idx).getUpdateConnectionStatistics();
            epsConnectionProcessors.remove(idx);
            logger.info("Client disconnect from server");
            return updateConnectionStatistics;
        } else {
            unexpectedHandler.serverConnectionToBeCloseNotFound();
            return null;
        }
    }

}
