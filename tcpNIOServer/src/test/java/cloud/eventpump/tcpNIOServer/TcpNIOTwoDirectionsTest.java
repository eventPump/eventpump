package cloud.eventpump.tcpNIOServer;

import cloud.eventpump.common.buffers.PrimitiveConversions;
import cloud.eventpump.common.buffers.ReadMultiBuffer;
import cloud.eventpump.common.buffers.WriteMultiBuffer;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProcessor;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToLong;
import static org.junit.Assert.assertEquals;

public class TcpNIOTwoDirectionsTest implements TcpNIOConnectionProcessor {

    private int closeConnectionCount = 0;

    private TcpNIOServer server;
    private TcpNIOClient client;

    private ServerConnection serverConnection;
    private ClientConnection clientConnection;

    @Before
    public void before() throws IOException, InterruptedException {
        closeConnectionCount = 0;
        server = new TcpNIOServer(this, new UnexpectedHandler());
        server.start(new InetSocketAddress(9875));

        client = new TcpNIOClient();
        clientConnection = new ClientConnection();
        TcpNIOConnectionProvides clientProvides = client.init(new InetSocketAddress(9875), null, clientConnection);
        clientConnection.setTcpNIOConnectionProvides(clientProvides);
        Thread thread = new Thread( () -> {
            try {
                client.mainLoop(0) ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } ) ;
        thread.start();

        Thread.sleep(50);
    }

    @Test
    public void clientServerSmallFrameExchange() throws IOException, InterruptedException {
        clientConnection.send(1);
        Thread.sleep(2);
        assertEquals(1, serverConnection.getReceivedCounter());
        Thread.sleep(13);

        clientConnection.send(1);
        Thread.sleep(2);
        assertEquals(2, serverConnection.getReceivedCounter());
        Thread.sleep(5);

        clientConnection.send(1);
        Thread.sleep(2);
        assertEquals(3, serverConnection.getReceivedCounter());

        client.close();
        server.close();
    }

    @Test
    public void serverClientSmallFrameExchange() throws IOException, InterruptedException {
        serverConnection.send(1);
        Thread.sleep(2);
        assertEquals(1, clientConnection.getReceivedCounter());
        Thread.sleep(13);

        serverConnection.send(1);
        Thread.sleep(2);
        assertEquals(2, clientConnection.getReceivedCounter());
        Thread.sleep(5);

        serverConnection.send(1);
        Thread.sleep(2);
        assertEquals(3, clientConnection.getReceivedCounter());

        client.close();
        server.close();
    }

    @Test
    public void bothDirectionSmallFrameExchange() throws IOException, InterruptedException {
        serverConnection.send(10);

        serverConnection.send(1);
        clientConnection.send(1);
        Thread.sleep(5);
        assertEquals(11, clientConnection.getReceivedCounter());
        assertEquals(1, serverConnection.getReceivedCounter());
        Thread.sleep(13);

        serverConnection.send(1);
        clientConnection.send(1);
        Thread.sleep(5);
        assertEquals(12, clientConnection.getReceivedCounter());
        assertEquals(2, serverConnection.getReceivedCounter());
        Thread.sleep(5);

        serverConnection.send(1);
        clientConnection.send(1);
        Thread.sleep(2);
        assertEquals(13, clientConnection.getReceivedCounter());
        assertEquals(3, serverConnection.getReceivedCounter());

        client.close();
        server.close();
    }

    @Test
    public void bothDirectionHugeFrameExchange() throws IOException, InterruptedException {
        serverConnection.send(1000);
        clientConnection.send(2000);
        Thread.sleep(10);
        assertEquals(1000, clientConnection.getReceivedCounter());
        assertEquals(2000, serverConnection.getReceivedCounter());
        Thread.sleep(13);

        serverConnection.send(2000);
        clientConnection.send(3000);
        Thread.sleep(10);
        assertEquals(3000, clientConnection.getReceivedCounter());
        assertEquals(5000, serverConnection.getReceivedCounter());
        Thread.sleep(5);

        serverConnection.send(2000);
        clientConnection.send(2000);
        Thread.sleep(10);
        assertEquals(5000, clientConnection.getReceivedCounter());
        assertEquals(7000, serverConnection.getReceivedCounter());

        client.close();
        server.close();
    }

    @Test
    public void bothDirectionHugeAsymmetricFrameExchange() throws IOException, InterruptedException {
        serverConnection.send(100_000);

        clientConnection.send(2000);
        Thread.sleep(15);
        assertEquals(2000, serverConnection.getReceivedCounter());
        // assertTrue( clientConnection.getReceivedCounter() < 100_000);
        Thread.sleep(1);

        clientConnection.send(3000);
        Thread.sleep(10);
        assertEquals(5000, serverConnection.getReceivedCounter());
        Thread.sleep(1);

        clientConnection.send(1000);
        Thread.sleep(10);
        assertEquals(6000, serverConnection.getReceivedCounter());
        Thread.sleep(1);

        assertEquals(100_000, clientConnection.getReceivedCounter());

        client.close();
        server.close();
    }


    @Override
    public TcpNIOConnectionRequires createConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        return serverConnection = new ServerConnection(tcpNIOConnectionProvides);
    }

    @Override
    public void closeConnection(TcpNIOConnectionRequires tcpNIOConnectionRequires) {
        closeConnectionCount++;
    }


    class ServerConnection implements TcpNIOConnectionRequires {

        private final TcpNIOConnectionProvides tcpNIOConnectionProvides;
        private final ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        private final WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(10 * 1024);

        private int sentCounter = 1;
        private volatile int receivedCounter = 0;

        public ServerConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
            this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        }

        @Override
        public void received(ByteBuffer byteBuffer) {
            readMultiBuffer.append(byteBuffer);
            while (readMultiBuffer.getSize() >= 8) {
                long receivedValue = bytesToLong(readMultiBuffer.getBytes(8));
                assertEquals(receivedCounter+1, receivedValue);
                receivedCounter++;
            }
        }

        @Override
        public void interrupted() {
        }

        public void send(int count) {
            for (int i = 0; i < count; i++) {
                writeMultiBuffer.putBytes(PrimitiveConversions.longToBytes(sentCounter++));
            }
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }

        public int getReceivedCounter() {
            return receivedCounter;
        }
    }

    class ClientConnection implements TcpNIOConnectionRequires {

        private TcpNIOConnectionProvides tcpNIOConnectionProvides;

        private ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        private WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(10 * 1024);

        private int sendCounter = 1;
        private volatile int receivedCounter = 0;

        public void setTcpNIOConnectionProvides(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
            this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        }

        public int getReceivedCounter() {
            return receivedCounter;
        }

        @Override
        public void received(ByteBuffer byteBuffer) {
            readMultiBuffer.append(byteBuffer);
            while (readMultiBuffer.getSize() >= 8) {
                long receivedValue = bytesToLong(readMultiBuffer.getBytes(8));
                assertEquals(receivedCounter+1, receivedValue);
                receivedCounter++;
            }
        }

        @Override
        public void interrupted() {
        }

        public void send(int count) {
            for (int i = 0; i < count; i++) {
                writeMultiBuffer.putBytes(PrimitiveConversions.longToBytes(sendCounter++));
            }
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }
    }
}