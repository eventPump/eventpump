package cloud.eventpump.common.marshaller;

import cloud.eventpump.common.frame.OutputFrameBuilder;

public class Marshaller {

    protected final OutputFrameBuilder outputFrameBuilder;

    public Marshaller(OutputFrameBuilder outputFrameBuilder) {
        this.outputFrameBuilder = outputFrameBuilder;
    }
}
