package cloud.eventpump.tests.common;

import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpClient.connection.marshaller.ClientMarshaller;
import cloud.eventpump.tcpClient.connection.unmarshaller.ClientUnmarshaller;
import cloud.eventpump.tcpServer.connection.malshaller.ServerMarshaller;
import cloud.eventpump.tcpServer.connection.unmarshaller.ServerUnmarshaller;
import cloud.eventpump.tcpServer.connection.unmarshaller.UnmarshallerProcessors;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MarshallerUnmarshallerTest {

    private long dbSequence;
    private long ack;
    private String clientName;

    private List<TEvent> pEvents;

    @Test
    public void serverUnmarshallerTest() {
        dbSequence = ack = -1;
        ServerUnmarshaller serverUnmarshaller = new ServerUnmarshaller(new UnmarshallerProcessors() {
            @Override
            public void startDBId(long dbId) {
                MarshallerUnmarshallerTest.this.dbSequence = dbId;
            }

            @Override
            public void clientName(String clientName) {
                MarshallerUnmarshallerTest.this.clientName = clientName;
            }

            @Override
            public void ack(long ack) {
                MarshallerUnmarshallerTest.this.ack = ack;
            }

            @Override
            public void ping() {
            }
        });


        ClientMarshaller clientMarshaller = new ClientMarshaller(100);

        List<ByteBuffer> byteBuffers = new ArrayList<>() ;
        byteBuffers.addAll(clientMarshaller.marshallAck(0xfcfa0102fcfa0102L));
        byteBuffers.addAll(clientMarshaller.marshallInit(0x1c110102111a0102L, "client-37"));
        byteBuffers.forEach( serverUnmarshaller::processReadData );
        assertEquals(0xfcfa0102fcfa0102L, ack);
        assertEquals(0x1c110102111a0102L, dbSequence);
        assertEquals("client-37", clientName);
        byteBuffers.clear();

        byteBuffers.addAll(clientMarshaller.marshallAck(0x31L));
        byteBuffers.addAll(clientMarshaller.marshallInit(0x41L, "client-56"));
        byteBuffers.forEach( serverUnmarshaller::processReadData );
        assertEquals(0x31L, ack);
        assertEquals(0x41L, dbSequence);
        assertEquals("client-56", clientName);
        byteBuffers.clear();

        byteBuffers.addAll(clientMarshaller.marshallAck(0x33L));
        byteBuffers.forEach( serverUnmarshaller::processReadData );
        assertEquals(0x33L, ack);
        byteBuffers.clear();
    }

    @Test
    public void serverMarshallerClientUnmarshallerTest() {
        ClientUnmarshaller<TEvent> clientUnmarshaller = new ClientUnmarshaller<>(
                new JacksonUnmarshaller<>(TEvent.class),
                pEvents -> this.pEvents = pEvents);

        ArrayList<ByteBuffer> byteBuffers = new ArrayList<>() ;
        ServerMarshaller<TEvent> serverMarshaller = new ServerMarshaller<>(new JacksonMarshaller<>(), 1024, new UnexpectedHandler());
        byteBuffers.addAll(serverMarshaller.marshallEvents(Arrays.asList(
                new TEvent(1, "ab"),
                new TEvent(2, "cde"))));
        byteBuffers.forEach(clientUnmarshaller::processReadData);

        assertEquals(2, pEvents.size());
        assertEquals(1, pEvents.get(0).getDbSequence());
        assertEquals("ab", pEvents.get(0).getContent());
        assertEquals(2, pEvents.get(1).getDbSequence());
        assertEquals("cde", pEvents.get(1).getContent());
        byteBuffers.clear();

        byteBuffers.addAll(serverMarshaller.marshallEvents(Arrays.asList(
                new TEvent(3, "ef"),
                new TEvent(4, "g"),
                new TEvent(5, "hij"))));
        byteBuffers.forEach(clientUnmarshaller::processReadData);

        assertEquals(3, pEvents.size());
        assertEquals(3, pEvents.get(0).getDbSequence());
        assertEquals("ef", pEvents.get(0).getContent());
        assertEquals(4, pEvents.get(1).getDbSequence());
        assertEquals("g", pEvents.get(1).getContent());
        assertEquals(5, pEvents.get(2).getDbSequence());
        assertEquals("hij", pEvents.get(2).getContent());
        byteBuffers.clear();
    }


}