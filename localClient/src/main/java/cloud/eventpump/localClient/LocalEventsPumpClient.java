package cloud.eventpump.localClient;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.EPSClientHandle;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.connection.EPSConnectionProvides;
import cloud.eventpump.server.connection.EPSConnectionRequires;

import java.util.List;

public class LocalEventsPumpClient<Event> implements EPSConnectionRequires<Event> {

    private final static String LOCAL_EVENT_PUMP_CLIENT_THREAD_NAME = "LocalEventsPumpClient";

    private final EventsApplier<Event> eventsApplier;
    private final EventsPumpServer<Event> eventsPumpServer;
    private final LocalClientConfig localClientConfig;
    private final UnexpectedHandler unexpectedHandler;

    private EPSConnectionProvides epDataConnectionProvides;
    private boolean commitOccurred = false;
    private boolean ackOccurred = false;
    private long ack = -1;
    private boolean closeRequired = false;

    public LocalEventsPumpClient(EventsApplier<Event> eventsApplier, EventsPumpServer<Event> eventsPumpServer, LocalClientConfig localClientConfig) {
        this.eventsApplier = eventsApplier;
        this.eventsPumpServer = eventsPumpServer;
        this.localClientConfig = localClientConfig;
        this.unexpectedHandler = localClientConfig.getUnexpectedHandler();
    }

    public void start() {
        Thread th = new Thread(this::mainClientLoop, LOCAL_EVENT_PUMP_CLIENT_THREAD_NAME + "-" + System.currentTimeMillis());
        th.start();
    }

    public synchronized void close() {
        closeRequired = true;
        notify();
    }

    public synchronized void ack(long ack) {
        if (ack <= 0 || ack <= this.ack) {
            localClientConfig.getUnexpectedHandler().badAckValueLocalClient(this.ack, ack);
        } else {
            this.ack = ack;
            ackOccurred = true;
            notify();
        }
    }

    private synchronized void commitOccurred() {
        commitOccurred = true;
        notify();
    }

    @Override
    public void processEvents(List<Event> events) {
        long ack = eventsApplier.applyEvents(events);
        if (ack >= 0) {
            ack(ack);
        }
    }

    private synchronized Action waitForActions() {
        for (; ; ) {
            if (closeRequired) {
                return null;
            }
            if (commitOccurred) {
                commitOccurred = false;
                return new CommitAction();
            }
            if (ackOccurred) {
                ackOccurred = false;
                return new AckAction(ack);
            }
            try {
                wait();
            } catch (InterruptedException ex) {
                // intentionally empty
            }
        }
    }

    private void mainClientLoop() {
        try {
            EPSClientHandle<Event> epClient = eventsPumpServer.getEPSClientHandle();
            epClient.appendCommitEventConsumer(this::commitOccurred);

            epDataConnectionProvides = epClient.createConnection(this);
            epDataConnectionProvides.setClientName(localClientConfig.getClientName());

            long lastAppliedDBId = eventsApplier.findLastAppliedDBId();
            epDataConnectionProvides.setStartDBId(lastAppliedDBId);

            Action action;
            while ((action = waitForActions()) != null) {
                action.execute();
            }
        } catch (Exception ex) {
            unexpectedHandler.localClientUnexpectedException(ex);
        }
    }


    private interface Action {
        void execute();
    }

    private class CommitAction implements Action {

        @Override
        public void execute() {
            epDataConnectionProvides.processCommit();
        }
    }

    private class AckAction implements Action {

        final long ack;

        AckAction(long ack) {
            this.ack = ack;
        }

        @Override
        public void execute() {
            epDataConnectionProvides.ack(ack);
        }
    }

}
