package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

public class MarshallerErrorsTest {

    @Test
    public void notSerializableEventOnServerTest() throws IOException, InterruptedException {
        EventsPumpServer<NotSerializableEvent> eventEventsPumpServer = new EventsPumpServer<>(
                (dbId, maxEventCount) -> Arrays.asList(new NotSerializableEvent(1)),
                NotSerializableEvent::getId, new ServerConfig());


        TcpServerConfig<NotSerializableEvent> tcpServerConfig = new TcpServerConfig<>( new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress( new InetSocketAddress(5678));
        TcpEventsPumpServer<NotSerializableEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        TcpClientConfig<NotSerializableEvent> tcpClientConfig = TcpClientConfig.create( new JacksonUnmarshaller<>(NotSerializableEvent.class));
        tcpClientConfig.setServer(new InetSocketAddress(5678));
        TcpEventsPumpClient<NotSerializableEvent> tcpEventsPumpClientRemote = new TcpEventsPumpClient<>(new EventsApplier<NotSerializableEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<NotSerializableEvent> notSerializableEvents) {
                return 1;
            }
        }, tcpClientConfig);
        tcpEventsPumpClientRemote.start();

        Thread.sleep(100);

        tcpEventsPumpClientRemote.close();
        tcpEventsPumpServer.close();
    }

    @Test
    public void differentTypesOfEventInClientAndServerTest() throws IOException, InterruptedException {
        EventsPumpServer<TEvent> eventEventsPumpServer = new EventsPumpServer<>(
                (dbId, maxEventCount) -> Arrays.asList(new TEvent(1, "a")),
                TEvent::getDbSequence, new ServerConfig());

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>( new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress( new InetSocketAddress(5678));
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        TcpClientConfig<PEvent> tcpClientConfig = TcpClientConfig.create( new JacksonUnmarshaller<>(PEvent.class));
        tcpClientConfig.setServer(new InetSocketAddress(5678));
        TcpEventsPumpClient<PEvent> tcpEventsPumpClientRemote = new TcpEventsPumpClient<>(new EventsApplier<PEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<PEvent> pEvents) {
                return 1;
            }
        }, tcpClientConfig);
        tcpEventsPumpClientRemote.start();

        Thread.sleep(100);

        tcpEventsPumpClientRemote.close();
        tcpEventsPumpServer.close();
    }

    private class PEvent implements Serializable {
        private long id;

        public PEvent(long id) {
            this.id = id;
        }

        public long getId() {
            return id;
        }
    }


    private class NotSerializableEvent {
        private long id;

        public NotSerializableEvent(long id) {
            this.id = id;
        }

        public long getId() {
            return id;
        }
    }

}
