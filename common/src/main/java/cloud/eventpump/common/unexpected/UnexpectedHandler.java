package cloud.eventpump.common.unexpected;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnexpectedHandler {

    private final Logger logger = LoggerFactory.getLogger(UnexpectedHandler.class);

    public void setLastAppliedDBIdWithInappropriateValue(long dbId) {
        throw new CloseConnectionException("SetLastAppliedDBId with Inappropriate value dbId=" + dbId);
    }

    public void ackDBIdNotFoundInSentEvents(long dbId) {
        throw new CloseConnectionException("Ack dbId not found in sent events ids dbId=" + dbId);
    }

    public void providedEventDBIdNotIncreaseMonotonically(long lastAppliedDBId, long eventId) {
        throw new CloseAppException("Provided eventId not increase monotonically lastAppliedDBId=" + lastAppliedDBId + " eventId=" + eventId);
    }

    public void serverConnectionToBeCloseNotFound() {
        logger.warn("Server connection to be close not found");
    }

    public void tooManyEventsProvided(int maxCount, int count) {
        logger.warn("Too many events provided maxCount=" + maxCount + " count=" + count + " truncated");
    }

    public void exceptionCaughtDuringProvideEvents(Exception ex) {
        logger.warn("Exception caught during provide events", ex);
    }

    public void badAckValueLocalClient(long previousAck, long ack) {
        logger.warn("Bad ack value previousAck=" + previousAck + " akc=" + ack);
    }

    public void unexpectedExceptionInTcpClientThread( Exception ex) {
        logger.error("TcpClient processing stopped because of unexpected exception", ex);
    }

    public boolean exceptionDuringConnect(Exception ex) {
        logger.warn( "TcpClient exception during connect (continue processing), exception message: " + ex.getMessage() );
        return true ; // repeat after any exception
    }

    public boolean clientExceptionDuringDataExchange(Exception ex) {
        if ( derivesFrom( ex, CloseConnectionException.class )) {
            logger.warn("Exception occurred (not fatal reconnecting)" + ex.getMessage());
            return true ;
        } else {
            logger.error("Fatal exception occurred (stopping client)", ex);
            return false;
        }
    }

    public void sleepInterruptedException( InterruptedException ex ) {
        logger.warn("Sleep interrupted exception ", ex);
    }

    private boolean derivesFrom(Exception ex, Class<? extends Exception> cmpClass) {
        Class<?> exClass = ex.getClass();
        do {
            if ( exClass.equals( cmpClass )) {
                return true ;
            }
            exClass = exClass.getSuperclass() ;
        } while ( exClass != null ) ;
        return false;
    }

    @Override
    public String toString() {
        return "UnexpectedHandler{}";
    }

    public void serverCloseConnectionException(CloseConnectionException ex) {
        logger.warn("Exception closing connection message : " + ex.getMessage());
    }

    public void serverCloseAppException(CloseAppException ex) {
        logger.error("Exception closing app", ex);
    }

    public void localClientUnexpectedException(Exception ex) {
        logger.error("LocalClient processing stopped because of unexpected exception", ex);
    }

    public void serverUnexpectedException(Exception ex) {
        logger.error("TcpServer processing stopped because of exception", ex);
    }
}
