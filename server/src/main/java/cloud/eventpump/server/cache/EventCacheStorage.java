package cloud.eventpump.server.cache;

import java.lang.reflect.Array;
import java.util.List;

public class EventCacheStorage<Event> {

    private final static int DEFAULT_CACHE_SIZE = 3;

    private final EventCacheItem<Event>[] items;

    public EventCacheStorage() {
        this(DEFAULT_CACHE_SIZE);
    }

    public EventCacheStorage(int cahceSize) {
        @SuppressWarnings("unchecked")
        EventCacheItem<Event>[] items = (EventCacheItem<Event>[]) Array.newInstance(EventCacheItem.class, cahceSize);
        this.items = items;
        init();
    }

    List<Event> get(long dbId) {
        int idx = find(dbId);
        if (idx == -1) {
            return null;
        }
        if (idx > 0) {
            moveToBegin(idx);
        }
        return items[0].events;
    }

    void put(long dbId, List<Event> events) {
        remove(items.length - 1);
        items[0] = new EventCacheItem<>(dbId, events);
    }

    private void init() {
        EventCacheItem<Event> emptyEventCacheItem = new EventCacheItem<>(-1, null);
        for (int i = 0; i < items.length; i++) {
            items[i] = emptyEventCacheItem;
        }
    }

    private void moveToBegin(int idx) {
        EventCacheItem<Event> tmp = items[idx];
        remove(idx);
        items[0] = tmp;
    }

    private void remove(int idx) {
        System.arraycopy(items, 0, items, 1, idx);
    }

    private int find(long dbId) {
        for (int i = 0; i < items.length; i++) {
            if (items[i].startDBId == dbId) {
                return i;
            }
        }
        return -1;
    }

}
