package cloud.eventpump.server.connection;


import cloud.eventpump.common.unexpected.CloseAppException;

class BufferedEventsInfo {

    private final EventDBIdTs[] eventDBIdTs;
    private int size = 0;
    private int readIdx = 0;
    private int writeIdx = 0;

    BufferedEventsInfo(int capacity) {
        this.eventDBIdTs = new EventDBIdTs[capacity];
        for (int i = 0; i < eventDBIdTs.length; i++) {
            eventDBIdTs[i] = new EventDBIdTs();
        }
    }

    int getSize() {
        return size;
    }

    int getRemaining() {
        return eventDBIdTs.length - size;
    }

    void put(long eventId, long eventTS) {
        if (size >= eventDBIdTs.length) {
            throw new CloseAppException("Internal error, BufferedEventsInfo overloaded size=" + size);
        }
        eventDBIdTs[writeIdx].set(eventId, eventTS);
        writeIdx = (writeIdx + 1) % eventDBIdTs.length;
        size++;
    }

    EventDBIdTs removeUntil(long id) {
        if (size <= 0) {
            return null;
        }
        final int idx = binarySearch(id);
        if (idx < 0) {
            return null;
        }
        size -= idx - readIdx + ((idx < readIdx) ? eventDBIdTs.length : 0) + 1;
        readIdx = (idx + 1) % eventDBIdTs.length;
        return eventDBIdTs[idx];
    }


    private int binarySearch(long key) {
        int lowIdx = readIdx;
        int highIdx = writeIdx - 1;
        if (highIdx < lowIdx) {
            highIdx += eventDBIdTs.length;
        }
        while (lowIdx <= highIdx) {
            int midIdx = (lowIdx + highIdx) >>> 1;
            EventDBIdTs midVal = eventDBIdTs[midIdx % eventDBIdTs.length];
            if (midVal.id < key)
                lowIdx = midIdx + 1;
            else if (midVal.id > key)
                highIdx = midIdx - 1;
            else
                return midIdx % eventDBIdTs.length; // found
        }
        return -1; // not found
    }

}



