package cloud.eventpump.server;

public interface EventAdapter<Event> {

    long getEventId(Event event);

    default long getTimestampUTC(Event event) {
        return -1 ;
    }

}
