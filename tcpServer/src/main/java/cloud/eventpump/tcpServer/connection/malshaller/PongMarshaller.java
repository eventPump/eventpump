package cloud.eventpump.tcpServer.connection.malshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.frame.OutputFrameBuilder;
import cloud.eventpump.common.marshaller.Marshaller;

public class PongMarshaller<Event> extends Marshaller {

    public PongMarshaller(OutputFrameBuilder outputFrameBuilder) {
        super(outputFrameBuilder);
    }

    public void pong() {
        outputFrameBuilder.startFrame(FrameNo.PONG_v100);
        outputFrameBuilder.finishFrame();
    }

}
