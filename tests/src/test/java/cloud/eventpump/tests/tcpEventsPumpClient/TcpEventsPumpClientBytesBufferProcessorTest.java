package cloud.eventpump.tests.tcpEventsPumpClient;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.connection.ConnectionProcessor;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpServer.connection.malshaller.ServerMarshaller;
import cloud.eventpump.tcpServer.connection.unmarshaller.ServerUnmarshaller;
import cloud.eventpump.tcpServer.connection.unmarshaller.UnmarshallerProcessors;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TcpEventsPumpClientBytesBufferProcessorTest {

    private List<TEvent> appliedPEvents = null;
    private ServerUnmarshaller serverUnmarshaller = null;

    private long ack = -1;
    private long dbSequence = -1;
    private String clientName = null;

    @Test
    public void bytesBufferProcessorTest() {
        ServerUnmarshaller serverUnmarshaller = new ServerUnmarshaller(new UnmarshallerProcessors() {
            @Override
            public void startDBId(long dbId) {
                TcpEventsPumpClientBytesBufferProcessorTest.this.dbSequence = dbId;
            }

            @Override
            public void ack(long dbId) {
                TcpEventsPumpClientBytesBufferProcessorTest.this.ack = dbId;
            }

            @Override
            public void ping() {
            }

            @Override
            public void clientName(String clientName) {
                TcpEventsPumpClientBytesBufferProcessorTest.this.clientName = clientName;
            }
        });

        TcpClientConfig<TEvent> tcpClientConfig = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        ConnectionProcessor<TEvent> connectionProcessor = new ConnectionProcessor<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return -1;
            }

            @Override
            public long applyEvents(List<TEvent> events) {
                appliedPEvents = events;
                return -1;
            }
        }, tcpClientConfig.getEventUnmarshaller(), new UnexpectedHandler(), tcpClientConfig.getWriteBufferSize(),10000);
        connectionProcessor.setTcpNIOConnectionProvides(new TcpNIOConnectionProvides() {
            @Override
            public void send(List<ByteBuffer> byteBuffers) {
                byteBuffers.forEach(bb -> serverUnmarshaller.processReadData(bb));
            }
        });
        assertEquals(-1, dbSequence);
        assertEquals(-1, ack);

        connectionProcessor.init(10, "client-333");

        assertEquals(10, dbSequence);
        assertEquals("client-333", clientName);
        assertEquals(-1, ack);
        dbSequence = -1;
        clientName = null;

        List<ByteBuffer> byteBuffers = new ArrayList<>();
        ServerMarshaller<TEvent> serverMarshaller = new ServerMarshaller<>(new JacksonMarshaller<>(), 1024, new UnexpectedHandler());
        byteBuffers.addAll(serverMarshaller.marshallEvents(Arrays.asList(
                new TEvent(11, "aaa"),
                new TEvent(12, "bbb"))));
        byteBuffers.forEach(connectionProcessor::received);

        assertEquals(-1, dbSequence);
        assertEquals(-1, ack);
        assertEquals(null, clientName);
        assertEquals(2, appliedPEvents.size());
        assertEquals("aaa", appliedPEvents.get(0).getContent());
        assertEquals("bbb", appliedPEvents.get(1).getContent());
        appliedPEvents = null;

        // ack 11
        connectionProcessor.ack(11);
        assertEquals(-1, dbSequence);
        assertEquals(11, ack);
        assertEquals(null, clientName);
        assertEquals(null, appliedPEvents);

        // ack 12
        connectionProcessor.ack(12);
        assertEquals(-1, dbSequence);
        assertEquals(12, ack);
        assertEquals(null, clientName);
        assertEquals(null, appliedPEvents);
        byteBuffers.clear();
    }

}