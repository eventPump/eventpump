package cloud.eventpump.tcpServer.connection.malshaller;


import cloud.eventpump.common.marshaller.EventMarshaller;
import cloud.eventpump.common.marshaller.MarshallerHost;
import cloud.eventpump.common.unexpected.UnexpectedHandler;

import java.nio.ByteBuffer;
import java.util.List;

public class ServerMarshaller<Event> {

    private final MarshallerHost marshallerHost;
    private final EventsMarshaller<Event> eventsMarshaller;
    private final PongMarshaller pongMarshaller ;

    public ServerMarshaller(EventMarshaller<Event> eventMarshaller, int writeBufferSize, UnexpectedHandler unexpectedHandler) {
        this.marshallerHost = new MarshallerHost(writeBufferSize);
        this.eventsMarshaller = new EventsMarshaller<>(marshallerHost.getOutputFrameBuilder(), eventMarshaller, unexpectedHandler);
        this.pongMarshaller = new PongMarshaller(marshallerHost.getOutputFrameBuilder()) ;
    }

    public List<ByteBuffer> marshallEvents(List<Event> pEvents) {
        eventsMarshaller.marshall(pEvents);
        return marshallerHost.getByteBuffers();
    }

    public List<ByteBuffer> marshallPong() {
        pongMarshaller.pong();
        return marshallerHost.getByteBuffers();
    }

}
