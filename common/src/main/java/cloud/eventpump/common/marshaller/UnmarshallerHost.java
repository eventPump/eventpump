package cloud.eventpump.common.marshaller;


import cloud.eventpump.common.buffers.ReadMultiBuffer;
import cloud.eventpump.common.frame.InputFrameCollector;
import cloud.eventpump.common.unexpected.CloseConnectionException;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class UnmarshallerHost {

    private final ReadMultiBuffer readMultiBuffer;
    private final InputFrameCollector inputFrameCollector;
    private final Map<Integer, Unmarshaller> frameNoToUnmarshaller;

    public UnmarshallerHost() {
        this.readMultiBuffer = new ReadMultiBuffer();
        this.inputFrameCollector = new InputFrameCollector(readMultiBuffer);
        this.frameNoToUnmarshaller = new HashMap<>();
    }

    protected void registerUnmarshaller(int frameNo, Unmarshaller unmarshaller) {
        frameNoToUnmarshaller.put(frameNo, unmarshaller);
    }

    public void processReadData(ByteBuffer byteBuffer) {
        readMultiBuffer.append(byteBuffer);
        unmarshallFrames();
    }

    private void unmarshallFrames() {
        int frameNo;
        while ((frameNo = inputFrameCollector.getNextFrameNo()) != -1) {
            unmarshallFrame(frameNo);
            inputFrameCollector.markFrameFinished();
        }
        frameNoToUnmarshaller.values().forEach(Unmarshaller::flush);
    }

    private void unmarshallFrame(int frameNo) {
        Unmarshaller unmarshaller = frameNoToUnmarshaller.get(frameNo);
        if (unmarshaller != null) {
            unmarshaller.unmarshall(inputFrameCollector);
        } else {
            throw new CloseConnectionException("UnrecognizedFrameNo frameNo = " + frameNo) ;
        }
    }

}
