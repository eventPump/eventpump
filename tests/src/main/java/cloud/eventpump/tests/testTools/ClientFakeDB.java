package cloud.eventpump.tests.testTools;

import cloud.eventpump.common.EventsApplier;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClientFakeDB implements EventsApplier<TEvent> {

    List<TEvent> events = new ArrayList<>();
    List<List<TEvent>> addedEvents = new ArrayList<>();

    private boolean autoAck = false;

    @Override
    public long findLastAppliedDBId() {
        if (events.isEmpty()) {
            return 0;
        } else {
            return events.get(events.size() - 1).getDbSequence();
        }
    }

    @Override
    public long applyEvents(List<TEvent> events) {
        addedEvents.add(events);
        try {
            for (TEvent event : events) {
                // System.out.println("Event received: " + event.getDbSequence() + " " + new String(event.getContent()));
                this.events.add(event);
                Thread.sleep(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (autoAck) {
            return events.get(events.size() - 1).getDbSequence();
        } else {
            return -1;
        }
    }

    public List<TEvent> getEvents() {
        return events;
    }

    public String strContent() {
        return events.stream().map(TEvent::getContent).map(String::new).collect(Collectors.joining("\n"));
    }

    public List<List<TEvent>> getLastWrites() {
        return addedEvents;
    }

    public void clearLastWrites() {
        addedEvents.clear();
    }

    public void setAutoAck(boolean autoAck) {
        this.autoAck = autoAck;
    }
}
