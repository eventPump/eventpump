package cloud.eventpump.tcpServer.connection.unmarshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.marshaller.UnmarshallerHost;

public class ServerUnmarshaller extends UnmarshallerHost {

    public ServerUnmarshaller(UnmarshallerProcessors unmarshallerProcessors) {
        registerUnmarshaller(FrameNo.INIT_v0100, new InitUnmarshaller(unmarshallerProcessors));
        registerUnmarshaller(FrameNo.ACK_v0100, new AckUnmarshaller(unmarshallerProcessors));
        registerUnmarshaller(FrameNo.PING_v0100, new PingUnmarshaller(unmarshallerProcessors));
    }

}
