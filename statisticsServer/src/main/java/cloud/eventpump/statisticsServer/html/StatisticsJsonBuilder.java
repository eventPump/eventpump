package cloud.eventpump.statisticsServer.html;

import cloud.eventpump.statisticsServer.ServerStatistics;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class StatisticsJsonBuilder {

    private final ObjectMapper mapper = new ObjectMapper();


    public String build(ServerStatistics statistics) throws JsonProcessingException {
        return mapper.writeValueAsString(statistics) ;
    }
}
