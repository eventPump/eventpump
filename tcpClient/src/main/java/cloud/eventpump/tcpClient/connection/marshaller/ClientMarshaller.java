package cloud.eventpump.tcpClient.connection.marshaller;


import cloud.eventpump.common.marshaller.MarshallerHost;

import java.nio.ByteBuffer;
import java.util.List;

public class ClientMarshaller {

    private final MarshallerHost marshallerHost;
    private final InitMarshaller initMarshaller;
    private final AckMarshaller ackMarshaller;
    private final PingMarshaller pingMarshaller;

    public ClientMarshaller(int writeBufferSize) {
        this.marshallerHost = new MarshallerHost(writeBufferSize);
        this.initMarshaller = new InitMarshaller(marshallerHost.getOutputFrameBuilder());
        this.ackMarshaller = new AckMarshaller(marshallerHost.getOutputFrameBuilder());
        this.pingMarshaller = new PingMarshaller(marshallerHost.getOutputFrameBuilder());
    }

    public List<ByteBuffer> marshallInit(long startDBId, String clientName) {
        initMarshaller.init(startDBId, clientName);
        return marshallerHost.getByteBuffers();
    }

    public List<ByteBuffer> marshallAck(long ack) {
        ackMarshaller.ack(ack);
        return marshallerHost.getByteBuffers();
    }

    public List<ByteBuffer> marshallPing() {
        pingMarshaller.ping();
        return marshallerHost.getByteBuffers();
    }

}
