package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.ClientFakeDB;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;

import static org.junit.Assert.*;

public class ContinuousConnectTest {

    @Test
    public void continuousConnectTest() throws IOException, InterruptedException {
        ClientFakeDB c1db = new ClientFakeDB();
        TcpClientConfig<TEvent> tcpClientConfig = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        tcpClientConfig.setClientName("c1");
        tcpClientConfig.setServer(new InetSocketAddress(5671));
        tcpClientConfig.setRepeatConnectionBasicDelay(10);
        tcpClientConfig.setRepeatConnectionMultiplier(1);
        tcpClientConfig.setKeepAliveFrameDelay(80);
        tcpClientConfig.setNoDataFromServerTimeout(300);
        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(c1db, tcpClientConfig);
        c1.startContinuous();

        Thread.sleep(2500);
        assertFalse( c1.isClientConnected() );

        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        serverFakeDB.setCommit(eventEventsPumpServer::commit);

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>(new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress(new InetSocketAddress(5671));
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();
        serverFakeDB.insert("a");

        Thread.sleep(500);
        assertTrue( c1.isClientConnected() );

        tcpEventsPumpServer.close();

        Thread.sleep(2500);
        assertFalse( c1.isClientConnected() );

        tcpEventsPumpServer.start();
        serverFakeDB.insert("b", "c");

        Thread.sleep(500);
        assertEquals(serverFakeDB.strContent(), c1db.strContent());
        assertTrue( c1.isClientConnected() );

        c1.close();
        tcpEventsPumpServer.close();
    }

}
