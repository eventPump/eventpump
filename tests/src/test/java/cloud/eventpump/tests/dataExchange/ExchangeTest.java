package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.localClient.LocalClientConfig;
import cloud.eventpump.localClient.LocalEventsPumpClient;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.ClientFakeDB;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ExchangeTest {

    private ServerFakeDB serverFakeDB;
    private EventsPumpServer<TEvent> eventEventsPumpServer;
    private TcpEventsPumpServer<TEvent> tcpEventsPumpServer;

    private ClientFakeDB clientFakeDB;
    private TcpEventsPumpClient<TEvent> tcpEventsPumpClientRemote;

    private LocalEventsPumpClient<TEvent> localEventsPumpClient;

    @Test
    public void oneCommitSlowReaderTCPTest() throws IOException, InterruptedException {
        setupTCP();
        checkOneCommitSlowReader();
        cleanTCP();
    }

    @Test
    public void oneCommitSlowReaderLocalTest() throws IOException, InterruptedException {
        setupLocal();
        checkOneCommitSlowReader();
        cleanLocal();
    }

    private void checkOneCommitSlowReader() throws InterruptedException {
        commit("abcdefg", 20, "abcd", "abcd");
        ack(1, 20, "", "");
        ack(2, 20, "ef", "ef");
        ack(3, 20, "", "");
        ack(4, 20, "g", "g");
        ack(5, 20, "", "");
        ack(6, 20, "", "");
        ack(7, 20, "", "");

        assertEquals(serverFakeDB.strContent(), clientFakeDB.strContent());
    }

//    @Test
//    public void manyCommitsSlowReaderTCPTest() throws IOException, InterruptedException {
//        setupTCP();
//        checkManyCommitsSlowReader();
//        cleanTCP();
//    }

    @Test
    public void manyCommitsSlowReaderLocalTest() throws IOException, InterruptedException {
        setupLocal();
        checkManyCommitsSlowReader();
        cleanLocal();
    }

    public void checkManyCommitsSlowReader() throws IOException, InterruptedException {
        commit("abcdefg", 20, "abcd", "abcd");
        ack(1, 20, "", "");
        ack(2, 20, "ef", "ef");
        ack(3, 20, "", "");
        ack(4, 20, "g", "g");
        ack(5, 20, "", "");
        ack(6, 20, "", "");
        ack(7, 20, "", "");
        commit("hi", 20, "hi", "hi");
        commit("jkl", 20, "jk", "jk");
        ack(8, 20, "", "");
        ack(9, 20, "l", "l");

        assertEquals(serverFakeDB.strContent(), clientFakeDB.strContent());
    }

    @Test
    public void overlappingCommitsSlowReaderTCPTest() throws IOException, InterruptedException {
        setupTCP();
        checkOverlappingCommitsSlowReader();
        cleanTCP();
    }

    @Test
    public void overlappingCommitsSlowReaderLocalTest() throws IOException, InterruptedException {
        setupLocal();
        checkOverlappingCommitsSlowReader();
        cleanLocal();
    }

    public void checkOverlappingCommitsSlowReader() throws IOException, InterruptedException {
        commit("abcdefg", 20, "abcd", "abcd");
        ack(1, 20, "", "");
        commit("hijkl", 20, "", "");
        ack(2, 20, "ef", "ef");
        ack(6, 20, "ghij", "ghij");
        ack(10, 20, "kl", "kl");
        ack(12, 20, "", "");

        assertEquals(serverFakeDB.strContent(), clientFakeDB.strContent());
    }

    @Test
    public void fastReaderTCPTest() throws IOException, InterruptedException {
        setupTCP();
        checkfastReader();
        cleanTCP();
    }

    @Test
    public void fastReaderLocalTest() throws IOException, InterruptedException {
        setupLocal();
        checkfastReader();
        cleanLocal();
    }

    public void checkfastReader() throws IOException, InterruptedException {
        commit("ab", 20, "ab", "ab");
        ack(2, 20, "", "");
        commit("c", 20, "c", "c");
        ack(3, 20, "", "");

        assertEquals(serverFakeDB.strContent(), clientFakeDB.strContent());
    }

    private void setupTCP() throws IOException, InterruptedException {
        serverFakeDB = new ServerFakeDB();
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setBufferingParameters(4, 2);
        eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), serverConfig );
        serverFakeDB.setCommit(eventEventsPumpServer::commit);

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>( new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress( new InetSocketAddress(5678));
        tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer,tcpServerConfig);
        tcpEventsPumpServer.start();

        clientFakeDB = new ClientFakeDB();
        TcpClientConfig<TEvent> tcpClientConfig = TcpClientConfig.create( new JacksonUnmarshaller<>(TEvent.class)) ;
        tcpClientConfig.setServer(new InetSocketAddress(5678));
        tcpEventsPumpClientRemote = new TcpEventsPumpClient<>(clientFakeDB, tcpClientConfig);
        tcpEventsPumpClientRemote.start();
        Thread.sleep(20);

        // The first hit to DB caused by setting startDBId
        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        assertEquals(1, lastReads.size());
        serverFakeDB.clearLastReads();
    }


    private void cleanTCP() throws InterruptedException, IOException {
        Thread.sleep(20);
        tcpEventsPumpServer.close();
        tcpEventsPumpClientRemote.close();
    }


    private void cleanLocal() throws InterruptedException {
        Thread.sleep(20);
        localEventsPumpClient.close();
    }

    private void setupLocal() throws IOException, InterruptedException {
        serverFakeDB = new ServerFakeDB();
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setBufferingParameters(4, 2);
        eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), serverConfig);
        serverFakeDB.setCommit(eventEventsPumpServer::commit);

        clientFakeDB = new ClientFakeDB();
        localEventsPumpClient = new LocalEventsPumpClient<>(clientFakeDB, eventEventsPumpServer, new LocalClientConfig());
        localEventsPumpClient.start();
        Thread.sleep(20);

        // The first hit to DB caused by setting startDBId
        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        assertEquals(1, lastReads.size());
        serverFakeDB.clearLastReads();
    }


    private void commit(String values, int sleep, String expectedRead, String expectedWrite) throws InterruptedException {
        String[] result = new String[values.length()];
        for (int i = 0; i < values.length(); i++) {
            result[i] = values.substring(i, i + 1);
        }
        serverFakeDB.insert(result);
        Thread.sleep(sleep + 100);

        checkDataBases(expectedRead, expectedWrite);
    }


    private void ack(int dbequence, int sleep, String expectedRead, String expectedWrite) throws InterruptedException {
        if (tcpEventsPumpClientRemote != null) {
            tcpEventsPumpClientRemote.ack(dbequence);
        } else {
            localEventsPumpClient.ack(dbequence);
        }
        Thread.sleep(sleep);

        checkDataBases(expectedRead, expectedWrite);
    }

    private void checkDataBases(String expectedRead, String expectedWrite) {
        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        if (!expectedRead.isEmpty()) {
            assertEquals(1, lastReads.size());
            assertEquals(expectedRead.length(), lastReads.get(0).size());
            serverFakeDB.clearLastReads();
        } else {
            assertEquals(0, lastReads.size());
        }

        List<List<TEvent>> lastWrites = clientFakeDB.getLastWrites();
        if (!expectedWrite.isEmpty()) {
            assertEquals(1, lastWrites.size());
            assertEquals(expectedWrite.length(), lastWrites.get(0).size());
            clientFakeDB.clearLastWrites();
        } else {
            assertEquals(0, lastWrites.size());
        }
    }

}
