package cloud.eventpump.tcpServer.connection.unmarshaller;


import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.Unmarshaller;

public class PingUnmarshaller implements Unmarshaller {

    private final UnmarshallerProcessors unmarshallerProcessors;

    public PingUnmarshaller(UnmarshallerProcessors unmarshallerProcessors) {
        this.unmarshallerProcessors = unmarshallerProcessors;
    }

    @Override
    public void unmarshall(PrimitivesReadBuffer primitivesReadBuffer) {
        unmarshallerProcessors.ping();
    }

}
