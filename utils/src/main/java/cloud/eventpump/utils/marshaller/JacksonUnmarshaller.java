package cloud.eventpump.utils.marshaller;

import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.EventUnmarshaller;
import cloud.eventpump.common.unexpected.CloseAppException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JacksonUnmarshaller<Event> implements EventUnmarshaller<Event> {

    private final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    private final Class<Event> eventClass;

    public JacksonUnmarshaller(Class<Event> eventClass) {
        this.eventClass = eventClass;
    }

    @Override
    public Event unmarshall(PrimitivesReadBuffer readBuffer) {
        try {
            return mapper.readValue(readBuffer.getString(), eventClass);
        } catch (IOException ex) {
            throw new CloseAppException(ex);
        }

    }
}
