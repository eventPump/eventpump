package cloud.eventpump.tcpClient;

import cloud.eventpump.common.marshaller.EventUnmarshaller;
import cloud.eventpump.common.unexpected.UnexpectedHandler;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

public class TcpClientConfig<Event> {

    private SocketAddress local = null;
    private SocketAddress server = new InetSocketAddress(9875);

    private String clientName = "remote-tcp-client";

    private EventUnmarshaller<Event> eventUnmarshaller ;

    private int writeBufferSize = 100;

    private int keepAliveFrameDelay = 5000;
    private int noDataFromServerTimeout = keepAliveFrameDelay * 5;

    private int repeatConnectionBasicDelay = 10000;
    private double repeatConnectionMultiplier = 2.0;
    private int repeatConnectionMaxDelay = 120000;

    private UnexpectedHandler unexpectedHandler = new UnexpectedHandler();


    public static <Event> TcpClientConfig<Event> create(EventUnmarshaller<Event> eventUnmarshaller) {
        return new TcpClientConfig<>(eventUnmarshaller);
    }

    TcpClientConfig(EventUnmarshaller<Event> eventUnmarshaller) {
        this.eventUnmarshaller = eventUnmarshaller;
    }

    public SocketAddress getLocal() {
        return local;
    }

    public void setLocal(SocketAddress local) {
        this.local = local;
    }

    public SocketAddress getServer() {
        return server;
    }

    public void setServer(SocketAddress server) {
        this.server = server;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getKeepAliveFrameDelay() {
        return keepAliveFrameDelay;
    }

    public void setKeepAliveFrameDelay(int keepAliveFrameDelay) {
        this.keepAliveFrameDelay = keepAliveFrameDelay;
    }

    public int getNoDataFromServerTimeout() {
        return noDataFromServerTimeout;
    }

    public void setNoDataFromServerTimeout(int noDataFromServerTimeout) {
        this.noDataFromServerTimeout = noDataFromServerTimeout;
    }

    public int getRepeatConnectionBasicDelay() {
        return repeatConnectionBasicDelay;
    }

    public void setRepeatConnectionBasicDelay(int repeatConnectionBasicDelay) {
        this.repeatConnectionBasicDelay = repeatConnectionBasicDelay;
    }

    public double getRepeatConnectionMultiplier() {
        return repeatConnectionMultiplier;
    }

    public void setRepeatConnectionMultiplier(double repeatConnectionMultiplier) {
        this.repeatConnectionMultiplier = repeatConnectionMultiplier;
    }

    public int getRepeatConnectionMaxDelay() {
        return repeatConnectionMaxDelay;
    }

    public void setRepeatConnectionMaxDelay(int repeatConnectionMaxDelay) {
        this.repeatConnectionMaxDelay = repeatConnectionMaxDelay;
    }

    public EventUnmarshaller<Event> getEventUnmarshaller() {
        return eventUnmarshaller;
    }

    public void setEventUnmarshaller(EventUnmarshaller<Event> eventUnmarshaller) {
        this.eventUnmarshaller = eventUnmarshaller;
    }

    public int getWriteBufferSize() {
        return writeBufferSize;
    }

    public void setWriteBufferSize(int writeBufferSize) {
        this.writeBufferSize = writeBufferSize;
    }

    public UnexpectedHandler getUnexpectedHandler() {
        return unexpectedHandler;
    }

    public void setUnexpectedHandler(UnexpectedHandler unexpectedHandler) {
        this.unexpectedHandler = unexpectedHandler;
    }

    @Override
    public String toString() {
        return "TcpClientConfig{" +
                "local=" + local +
                ", server=" + server +
                ", clientName='" + clientName + '\'' +
                ", eventUnmarshaller=" + eventUnmarshaller +
                ", writeBufferSize=" + writeBufferSize +
                ", keepAliveFrameDelay=" + keepAliveFrameDelay +
                ", noDataFromServerTimeout=" + noDataFromServerTimeout +
                ", repeatConnectionBasicDelay=" + repeatConnectionBasicDelay +
                ", repeatConnectionMultiplier=" + repeatConnectionMultiplier +
                ", repeatConnectionMaxDelay=" + repeatConnectionMaxDelay +
                ", unexpectedHandler=" + unexpectedHandler +
                '}';
    }
}
