package cloud.eventpump.common.buffers;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class WriteMultiBuffer  {

    // TODO better code, no repetition

    private final int writeBufferSize;
    private List<ByteBuffer> buffers;
    private int totalSize = 0;

    public WriteMultiBuffer(int writeBufferSize) {
        this.writeBufferSize = writeBufferSize;
        initBuffers();
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void putBytes(byte[] bytes) {
        for (int readPos = 0; readPos < bytes.length; ) {
            ByteBuffer writeBuffer = getWriteBuffer();
            int toWrite = Math.min(writeBuffer.remaining(), bytes.length - readPos);
            writeBuffer.put(bytes, readPos, toWrite);
            readPos += toWrite;
        }
        totalSize += bytes.length;
    }

    public PlaceholderInfo reservePlaceholder(int size) {
        int lastBufferIdx = buffers.size()-1 ;
        PlaceholderInfo placeholderInfo = new PlaceholderInfo(
                lastBufferIdx,
                buffers.get(lastBufferIdx).position(),
                size);
        putBytes(new byte[size]);
        return placeholderInfo ;
    }

    public void overwritePlaceholder(PlaceholderInfo placeholderInfo, byte[] bytes) {
        // TODO check == -1, check markedBufferSize != size ;
        for (int readPos = 0; readPos < bytes.length; ) {
            ByteBuffer writeBuffer = buffers.get(placeholderInfo.markedBufferIdx);
            int toWrite = Math.min(writeBuffer.limit() - placeholderInfo.markedBufferPos, bytes.length - readPos);
            put(writeBuffer, placeholderInfo.markedBufferPos, bytes, readPos, toWrite);
            placeholderInfo.markedBufferIdx++;
            placeholderInfo.markedBufferPos = 0;
            readPos += toWrite;
        }
    }

    public List<ByteBuffer> getByteBuffers() {
        List<ByteBuffer> tmp = buffers;
        initBuffers();
        tmp.forEach(ByteBuffer::flip);
        return tmp;
    }

    private void initBuffers() {
        buffers = new ArrayList<>();
        buffers.add(ByteBuffer.allocate(writeBufferSize));
    }

    private ByteBuffer getWriteBuffer() {
        ByteBuffer lastBuffer = buffers.get(buffers.size() - 1);
        if (lastBuffer.remaining() == 0) {
            lastBuffer = ByteBuffer.allocate(writeBufferSize);
            buffers.add(lastBuffer);
        }
        return lastBuffer;
    }

    private void put(ByteBuffer writeBuffer, int writePos, byte[] bytes, int readPos, int toWrite) {
        for (int i = 0; i < toWrite; i++) {
            writeBuffer.put(writePos + i, bytes[readPos + i]);
        }
    }

    public class PlaceholderInfo {
        private int markedBufferIdx;
        private int markedBufferPos;
        private int markedBufferSize;

        public PlaceholderInfo(int markedBufferIdx, int markedBufferPos, int markedBufferSize) {
            this.markedBufferIdx = markedBufferIdx;
            this.markedBufferPos = markedBufferPos;
            this.markedBufferSize = markedBufferSize;
        }
    }

}
