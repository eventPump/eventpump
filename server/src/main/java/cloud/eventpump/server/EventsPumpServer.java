package cloud.eventpump.server;


import cloud.eventpump.common.UpdateServerStatistics;
import cloud.eventpump.server.cache.EventCache;
import cloud.eventpump.server.eventsProviders.FrontEndEventsProvider;
import cloud.eventpump.server.eventsProviders.NothingToReadController;

public class EventsPumpServer<Event> {

    private final EventCache<Event> eventEventCache;
    private final NothingToReadController<Event> nothingToReadController;
    private final EPSClientHandler<Event> eventEPSClientHandler;

    private final ServerConfig config;

    public EventsPumpServer(EventsProvider<Event> eventsProvider, EventAdapter<Event> eventAdapter, ServerConfig config) {
        FrontEndEventsProvider<Event> frontEndEventsProvider = FrontEndEventsProvider.create(eventsProvider, config.getUnexpectedHandler());
        this.eventEventCache = EventCache.create(frontEndEventsProvider, eventAdapter);
        this.nothingToReadController = new NothingToReadController<>(eventEventCache);
        this.eventEPSClientHandler = new EPSClientHandler<>(
                nothingToReadController,
                eventAdapter,
                config.getMaxBufferedEventsCount(),
                config.getEventsReadThreshold(),
                config.getUnexpectedHandler());
        this.config = config;

    }

    public void setStatisticsServer(UpdateServerStatistics updateServerStatistics) {
        eventEventCache.setStatistics(updateServerStatistics);
        eventEPSClientHandler.setStatisticsServer(updateServerStatistics);
        updateServerStatistics.setServerName(config.getServerName());
    }

    public void commit() {
        nothingToReadController.clear();
        eventEPSClientHandler.commit();
    }

    public EPSClientHandle<Event> getEPSClientHandle() {
        return eventEPSClientHandler;
    }


}
