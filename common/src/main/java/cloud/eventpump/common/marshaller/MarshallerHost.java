package cloud.eventpump.common.marshaller;

import cloud.eventpump.common.buffers.WriteMultiBuffer;
import cloud.eventpump.common.frame.OutputFrameBuilder;

import java.nio.ByteBuffer;
import java.util.List;

public class MarshallerHost {

    private final WriteMultiBuffer writeMultiBuffer;
    private final OutputFrameBuilder outputFrameBuilder;

    public MarshallerHost(int writeBufferSize ) {
        writeMultiBuffer = new WriteMultiBuffer(writeBufferSize);
        outputFrameBuilder = new OutputFrameBuilder(writeMultiBuffer);
    }

    public OutputFrameBuilder getOutputFrameBuilder() {
        return outputFrameBuilder ;
    }

    public List<ByteBuffer> getByteBuffers() {
        return writeMultiBuffer.getByteBuffers();
    }

}
