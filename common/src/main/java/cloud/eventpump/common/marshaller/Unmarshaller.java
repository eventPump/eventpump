package cloud.eventpump.common.marshaller;


import cloud.eventpump.common.buffers.PrimitivesReadBuffer;

public interface Unmarshaller {

    void unmarshall(PrimitivesReadBuffer primitivesReadBuffer);

    default void flush() {
    }
}
