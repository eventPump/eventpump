package cloud.eventpump.tcpServer.connection.malshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.frame.OutputFrameBuilder;
import cloud.eventpump.common.marshaller.EventMarshaller;
import cloud.eventpump.common.marshaller.Marshaller;
import cloud.eventpump.common.unexpected.CloseAppException;
import cloud.eventpump.common.unexpected.UnexpectedHandler;

import java.util.List;

public class EventsMarshaller<Event> extends Marshaller {

    private final EventMarshaller<Event> eventMarshaller;
    private final UnexpectedHandler unexpectedHandler;

    public EventsMarshaller(OutputFrameBuilder outputFrameBuilder, EventMarshaller<Event> eventMarshaller, UnexpectedHandler unexpectedHandler) {
        super(outputFrameBuilder);
        this.eventMarshaller = eventMarshaller;
        this.unexpectedHandler = unexpectedHandler;
    }

    public void marshall(List<Event> events) {
        events.forEach(this::marshall);
    }

    private void marshall(Event event) {
        outputFrameBuilder.startFrame(FrameNo.DATA_v0100);
        try {
            eventMarshaller.marshall(event, outputFrameBuilder);
        } catch (Exception ex) {
            throw new CloseAppException(ex);
        }
        outputFrameBuilder.finishFrame();
    }

}
