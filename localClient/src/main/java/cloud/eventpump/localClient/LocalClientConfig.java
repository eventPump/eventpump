package cloud.eventpump.localClient;

import cloud.eventpump.common.unexpected.UnexpectedHandler;

public class LocalClientConfig {
    private String clientName = "local-client";
    private final UnexpectedHandler unexpectedHandler = new UnexpectedHandler();

    public LocalClientConfig() {
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public UnexpectedHandler getUnexpectedHandler() {
        return unexpectedHandler;
    }
}
