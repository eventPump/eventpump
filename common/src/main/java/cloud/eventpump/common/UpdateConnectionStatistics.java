package cloud.eventpump.common;

public interface UpdateConnectionStatistics {

    default void applyAck(long ackId, long ackTs, int bufferRemaining) {
    }

    default void applyReadEventsCount(int readEventsCount) {
    }

    default void setClientName(String clientName) {
    }

    default void applySentBytesCount(int sentBytesCount) {
    }

    default void applyReceivedBytesCount(int receivedBytesCount) {
    }

    default void setClientSynchronized(boolean clientSynchronized) {}
}
