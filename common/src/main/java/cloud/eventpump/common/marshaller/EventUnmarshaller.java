package cloud.eventpump.common.marshaller;

import cloud.eventpump.common.buffers.PrimitivesReadBuffer;

public interface EventUnmarshaller<Event> {
    Event unmarshall(PrimitivesReadBuffer readBuffer) throws Exception;
}
