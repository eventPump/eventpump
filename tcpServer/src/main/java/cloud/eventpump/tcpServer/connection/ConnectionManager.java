package cloud.eventpump.tcpServer.connection;


import cloud.eventpump.common.marshaller.EventMarshaller;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.EPSClientHandle;
import cloud.eventpump.server.connection.EPSConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProcessor;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;

import java.util.ArrayList;
import java.util.List;

public class ConnectionManager<Event> implements TcpNIOConnectionProcessor {

    private final EventMarshaller<Event> eventMarshaller;
    private final EPSClientHandle<Event> epClient;
    private final UnexpectedHandler unexpectedHandler;
    private int writeBufferSize ;

    private final List<ConnectionProcessor<Event>> connectionProcessors = new ArrayList<>();

    public ConnectionManager(EventMarshaller<Event> eventMarshaller, EPSClientHandle<Event> epClient, int writeBufferSize, UnexpectedHandler unexpectedHandler) {
        this.eventMarshaller = eventMarshaller;
        this.epClient = epClient;
        this.unexpectedHandler = unexpectedHandler;
        this.writeBufferSize = writeBufferSize ;
    }

    @Override
    public synchronized TcpNIOConnectionRequires createConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        ConnectionProcessor<Event> connectionProcessor = newConnectionProcessor(tcpNIOConnectionProvides);
        connectionProcessors.add(connectionProcessor);
        return connectionProcessor;
    }

    @Override
    public synchronized void closeConnection(TcpNIOConnectionRequires tcpNIOConnectionRequires) {
        int idx = connectionProcessors.indexOf(tcpNIOConnectionRequires);
        if (idx != -1) {
            epClient.closeConnection(connectionProcessors.get(idx).getEpConnection());
            connectionProcessors.remove(idx);
        } else {
            unexpectedHandler.serverConnectionToBeCloseNotFound();
        }
    }

    private ConnectionProcessor<Event> newConnectionProcessor(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        ConnectionProcessor<Event> connectionProcessor = new ConnectionProcessor<>(eventMarshaller, writeBufferSize, unexpectedHandler);
        EPSConnectionProvides epConnection = epClient.createConnection(connectionProcessor);
        connectionProcessor.init(epConnection, tcpNIOConnectionProvides);
        return connectionProcessor;
    }

}
