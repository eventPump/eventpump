package cloud.eventpump.tests.statisticsEventPumpServer;

import cloud.eventpump.localClient.LocalClientConfig;
import cloud.eventpump.localClient.LocalEventsPumpClient;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.ConnectionStatistics;
import cloud.eventpump.statisticsServer.ServerStatistics;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.statisticsServer.StatisticsWebConfig;
import cloud.eventpump.statisticsServer.StatisticsWebEventPumpServer;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.ClientFakeDB;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StatisticsEventPumpServerTest {

    private ServerFakeDB serverFakeDB;
    private EventsPumpServer<TEvent> eventEventsPumpServer;
    private TcpEventsPumpServer<TEvent> tcpEventsPumpServer;

    private StatisticsEventPumpServer<TEvent> statisticsEventPumpServer;
    private StatisticsWebEventPumpServer<TEvent> statisticsWebEventPumpServer;

    private ClientFakeDB tcpClientFakeDB;
    private TcpEventsPumpClient<TEvent> tcpEventsPumpClientRemote;

    private ClientFakeDB localClientFakeDB;
    private LocalEventsPumpClient<TEvent> localEventsPumpClient;

    @Before
    public void before() throws IOException, InterruptedException {
        serverFakeDB = new ServerFakeDB();
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setBufferingParameters(4, 2);
        eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), serverConfig);
        serverFakeDB.setCommit(eventEventsPumpServer::commit);

        statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>(new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress(new InetSocketAddress(5171));
        tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        tcpClientFakeDB = new ClientFakeDB();
        tcpClientFakeDB.setAutoAck(true);
        TcpClientConfig<TEvent> tcpClientConfig = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        tcpClientConfig.setServer(new InetSocketAddress(5171));
        tcpClientConfig.setClientName("remote-1");
        tcpEventsPumpClientRemote = new TcpEventsPumpClient<>(tcpClientFakeDB, tcpClientConfig);
        tcpEventsPumpClientRemote.start();

        Thread.sleep(100);

        localClientFakeDB = new ClientFakeDB();
        localClientFakeDB.setAutoAck(true);
        LocalClientConfig localClientConfig = new LocalClientConfig();
        localClientConfig.setClientName("local-1");
        localEventsPumpClient = new LocalEventsPumpClient<>(localClientFakeDB, eventEventsPumpServer, localClientConfig);
        localEventsPumpClient.start();

        StatisticsWebConfig statisticsWebConfig = new StatisticsWebConfig();
        statisticsWebConfig.setPort(6789);
        statisticsWebEventPumpServer = new StatisticsWebEventPumpServer<>(statisticsEventPumpServer, statisticsWebConfig);
        statisticsWebEventPumpServer.start();

        Thread.sleep(200);
    }

    @After
    public void after() {
        tcpEventsPumpServer.close();
        localEventsPumpClient.close();
        statisticsWebEventPumpServer.close();
    }

    @Test
    public void statisticsTest() throws InterruptedException {
        serverFakeDB.insert("v1");
        Thread.sleep(100);
        serverFakeDB.insert("v2");
        Thread.sleep(100);
        serverFakeDB.insert("v3");
        Thread.sleep(100);

        assertEquals(serverFakeDB.strContent(), localClientFakeDB.strContent());
        assertEquals(serverFakeDB.strContent(), tcpClientFakeDB.strContent());

        ServerStatistics statistics = statisticsEventPumpServer.getStatistics();

        assertEquals(3, statistics.getCacheHit());
        assertEquals(6, statistics.getCacheTotal());
        assertEquals(1, statistics.getNoEventsRead());

        List<? extends ConnectionStatistics> connectionStatistics = statistics.getConnectionStatistics();
        assertEquals(2, connectionStatistics.size());

        ConnectionStatistics remoteCS = connectionStatistics.get(0);
        assertEquals("remote-1", remoteCS.getClientName());
        assertTrue(0 < remoteCS.getClientLag());
        assertTrue(remoteCS.isClientSynchronized());
        assertEquals(3, remoteCS.getLastReceivedAckDbId());
        assertEquals(3, remoteCS.getTotalReadEventsCount());
        assertEquals(3, remoteCS.getTotalAckEventsCount());
        assertTrue(100 < remoteCS.getTotalSentBytesCount());
        assertTrue(50 < remoteCS.getTotalReceivedBytesCount());

        ConnectionStatistics localCS = connectionStatistics.get(1);
        assertEquals("local-1", localCS.getClientName());
        assertTrue(0 < localCS.getClientLag());
        assertTrue(remoteCS.isClientSynchronized());
        assertEquals(3, localCS.getLastReceivedAckDbId());
        assertEquals(3, localCS.getTotalReadEventsCount());
        assertEquals(3, localCS.getTotalAckEventsCount());
        assertEquals(0, localCS.getTotalSentBytesCount());
        assertEquals(0, localCS.getTotalReceivedBytesCount());

//        Thread.sleep(100);
//        Random random = new Random() ;
//        for( int i = 0 ; i < 100 ; i++ ) {
//            for ( int j = 0 ; j < random.nextInt(10000); j++ ) {
//                serverFakeDB.insert("v1");
//            }
//            Thread.sleep(1000);
//        }
    }


}