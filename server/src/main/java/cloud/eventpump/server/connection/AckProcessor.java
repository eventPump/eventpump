package cloud.eventpump.server.connection;

import cloud.eventpump.common.UpdateConnectionStatistics;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.EventAdapter;

import java.util.List;

class AckProcessor<Event> {

    private final EventAdapter<Event> eventEventAdapter;
    private final UpdateConnectionStatistics updateConnectionStatistics;
    private final BufferedEventsInfo bufferedEventsInfo;

    private final UnexpectedHandler unexpectedHandler;

    private long lastAppliedDBId = -1;

    public AckProcessor(EventAdapter<Event> eventEventAdapter, UpdateConnectionStatistics updateConnectionStatistics,
                        BufferedEventsInfo bufferedEventsInfo, UnexpectedHandler unexpectedHandler) {
        this.eventEventAdapter = eventEventAdapter;
        this.updateConnectionStatistics = updateConnectionStatistics;
        this.bufferedEventsInfo = bufferedEventsInfo;
        this.unexpectedHandler = unexpectedHandler;
    }

    long getLastAppliedDBId() {
        return lastAppliedDBId;
    }

    void setLastAppliedDBId(long dbId) {
        if (dbId >= 0) {
            this.lastAppliedDBId = dbId;
        } else {
            unexpectedHandler.setLastAppliedDBIdWithInappropriateValue(dbId);
        }
    }

    void ack(long dbId) {
        EventDBIdTs eventDBIdTs = bufferedEventsInfo.removeUntil(dbId);
        if (eventDBIdTs != null) {
            updateConnectionStatistics.applyAck(dbId, eventDBIdTs.ts, bufferedEventsInfo.getSize());
        } else {
            unexpectedHandler.ackDBIdNotFoundInSentEvents(dbId);
        }
    }

    int getPossibleReadCount() {
        return bufferedEventsInfo.getRemaining();
    }

    boolean isClientSynchronized() {
        return bufferedEventsInfo.getSize() == 0;
    }

    void applyInProgressEvents(List<Event> events) {
        events.forEach(this::applyInProgressEvent);
        updateConnectionStatistics.applyReadEventsCount(events.size());
    }

    private void applyInProgressEvent(Event event) {
        final long eventId = eventEventAdapter.getEventId(event);
        if (eventId <= lastAppliedDBId) {
            unexpectedHandler.providedEventDBIdNotIncreaseMonotonically(lastAppliedDBId, eventId);
        } else {
            lastAppliedDBId = eventId;
            bufferedEventsInfo.put(eventId, eventEventAdapter.getTimestampUTC(event));
        }
    }

}
