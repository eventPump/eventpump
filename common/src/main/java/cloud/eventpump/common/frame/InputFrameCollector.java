package cloud.eventpump.common.frame;


import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.buffers.ReadMultiBuffer;

import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToInt;
import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToLong;

public class InputFrameCollector implements PrimitivesReadBuffer {

    private ReadMultiBuffer readMultiBuffer;
    private int nextFrameLength = Integer.MAX_VALUE;
    private int nextFrameNo = -1;


    public InputFrameCollector(ReadMultiBuffer readMultiBuffer) {
        this.readMultiBuffer = readMultiBuffer;
    }

    public long getLong() {
        return bytesToLong(readMultiBuffer.getBytes(8));
    }

    public int getInt() {
        return bytesToInt(readMultiBuffer.getBytes(4));
    }

    public String getString() {
        int length = getInt();
        return new String(readMultiBuffer.getBytes(length));
    }

    @Override
    public byte[] getBytes() {
        int length = getInt();
        return readMultiBuffer.getBytes(length);
    }

    public int getNextFrameNo() {
        if (nextFrameLength == Integer.MAX_VALUE && readMultiBuffer.getSize() >= 4) {
            nextFrameLength = getInt();
        }
        if (nextFrameLength <= readMultiBuffer.getSize()) {
            nextFrameNo = getInt();
        }
        return nextFrameNo;
    }

    public void markFrameFinished() {
        nextFrameLength = Integer.MAX_VALUE;
        nextFrameNo = -1;
    }

}
