package cloud.eventpump.common;

import java.util.List;

public interface EventsApplier<Event> {

    long findLastAppliedDBId() ;

    long applyEvents(List<Event> events);

}
