package cloud.eventpump.tcpNIOServer.connection;

import cloud.eventpump.common.unexpected.CloseConnectionNIOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TcpNIOConnection implements TcpNIOConnectionProvides {

    private final static int READ_SELECTION_KEY = SelectionKey.OP_READ;
    private final static int READ_WRITE_SELECTION_KEY = SelectionKey.OP_READ | SelectionKey.OP_WRITE;

    private final Logger logger = LoggerFactory.getLogger(TcpNIOConnection.class);

    private final SocketChannel channel;
    private final SendInterruptible sendInterruptible;

    private TcpNIOConnectionRequires tcpNIOConnectionRequires;
    private List<ByteBuffer> writeBuffers = new LinkedList<>();
    private boolean readyToWrite = true;

    public TcpNIOConnection(SocketChannel channel, SendInterruptible sendInterruptible) {
        this.channel = channel;
        this.sendInterruptible = sendInterruptible;
    }

    public TcpNIOConnectionRequires getTcpNIOConnectionRequires() {
        return tcpNIOConnectionRequires;
    }

    public void setTcpNIOConnectionRequires(TcpNIOConnectionRequires connection) {
        this.tcpNIOConnectionRequires = connection;
    }

    @Override
    synchronized public void send(List<ByteBuffer> byteBuffers) {
        writeBuffers.addAll(byteBuffers);
        if (readyToWrite) {
            readyToWrite = false;
            sendInterruptible.wakeUpForSend();
        }
    }

    public boolean processReadyToRead()  {
        ByteBuffer readBuffer = ByteBuffer.allocate(1024 * 10);    // TODO: configure
        try {
            if (channel.read(readBuffer) < 0) {
                return false;
            }
        } catch (IOException ex) {
            throw new CloseConnectionNIOException(ex);
        }
        readBuffer.flip();
        tcpNIOConnectionRequires.received(readBuffer);
        return true;
    }

    // TODO synchronized should not be used
    synchronized public void processReadyToWrite() {
        if (writePending()) {
            ByteBuffer[] byteBuffers = prepareBuffersToWrite();
            try {
                channel.write(byteBuffers);
            } catch (Exception ex) {
                throw new CloseConnectionNIOException(ex);
            }
            removeProcessedBuffers();
            readyToWrite = !writePending();
        }
    }

    public void processCommit() {
        tcpNIOConnectionRequires.interrupted();
    }

    public void register(Selector selector) throws IOException {
        channel.register(selector, writePending() ? READ_WRITE_SELECTION_KEY : READ_SELECTION_KEY, this);
    }

    private boolean writePending() {
        return !writeBuffers.isEmpty();
    }

    private ByteBuffer[] prepareBuffersToWrite() {
        ByteBuffer[] byteBuffers = new ByteBuffer[Math.min(writeBuffers.size(), 10)];    // TODO better default, better solution
        for (int i = 0; i < byteBuffers.length; i++) {
            byteBuffers[i] = writeBuffers.get(i);
        }
        return byteBuffers;
    }

    private void removeProcessedBuffers() {
        ArrayList<ByteBuffer> tmp = new ArrayList<>();
        writeBuffers.forEach(ff -> {
            if (ff.remaining() > 0) {
                tmp.add(ff);
            }
        });
        writeBuffers = tmp;
    }


    public void close() throws IOException {
        channel.close();
    }
}
