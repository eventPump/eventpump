// https://divtable.com/table-styler/

const app = document.getElementById('root')

const pageHeader = document.createElement('h1')
pageHeader.textContent = 'EventPump Statistics'
app.appendChild( pageHeader)

// const logo = document.createElement('img')
// logo.src = 'logo.png'
// app.appendChild(logo)

var errorMessage = document.createElement('h1')
errorMessage.setAttribute('id','errorMessage')
errorMessage.style.color = 'red';
app.appendChild(errorMessage)

const container = document.createElement('div')
container.setAttribute('class', 'container')
app.appendChild(container)

function createElement( element, text ) {
    var element = document.createElement(element);
    element.innerHTML = text
    return element
}

function createTableRow( element, firstRowClass, text1, text2, text3, text4, text5, text6, text7, text8, text9, text10) {
    var tr = document.createElement('tr');
     for (var i = 2; i < arguments.length; i++) {
        var e1 = createElement(element, arguments[i])
        if ( i == 2 ) {
            e1.setAttribute('class', firstRowClass)
        }
        tr.appendChild( e1 )
      }
    return tr;
}

function createConnectionStatisticsTableHeader() {
    return createTableRow( 'th', 'empty', 'Name','Sync','Lag','Last Ack', 'Ack Count', 'Read Count',
    'Received Bytes', 'Sent Bytes', 'Received Ack Ts', 'Ack Ts')
}

function formatDateUTC( dateUTC ) {
    return new Date(dateUTC).toISOString().replace('T', ' ').replace('Z', '')
}

function formatClientSynchronized( clientSynchronized ) {
    if ( clientSynchronized ) {
        return '<font color="green"><b>&#10004;</b></font>'
    } else{
        return '<font color="red"><b>&#10008;</b></font>'
    }
}

function createConnectionStatisticsTableRow( s ) {
    return createTableRow( 'td', 'firstRow',
    s.clientName,
    formatClientSynchronized(s.clientSynchronized),
    s.clientLag.toLocaleString() + ' ms',
    s.lastReceivedAckDbId.toLocaleString(),
    s.totalAckEventsCount.toLocaleString(),
    s.totalReadEventsCount.toLocaleString(),
    s.totalReceivedBytesCount.toLocaleString(),
    s.totalSentBytesCount.toLocaleString(),
    formatDateUTC(s.lastReceivedAckEventUtcTs),
    formatDateUTC(s.lastReceivedAckUtcTs))
}

function createConnectionStatisticsTable( connectionStatistics ) {
    var table = document.createElement('table');
    table.setAttribute( 'class', 'blueTable')
    var thead = document.createElement('thead')
    thead.appendChild( createConnectionStatisticsTableHeader()) ;
    table.appendChild(thead)
    var tbody = document.createElement('tbody')
    connectionStatistics.forEach( cs => {
        tbody.appendChild(createConnectionStatisticsTableRow(cs))
    })
    table.appendChild(tbody)
    return table
}

function createServerStatisticsTable( serverStatistics ) {
    var table = document.createElement('table');
    table.setAttribute( 'class', 'blueTable')
    var thead = document.createElement('thead')
    thead.appendChild(createTableRow('th', 'empty', 'Label', 'Value'))
    table.appendChild(thead)
    var tbody = document.createElement('tbody')
    tbody.appendChild(createTableRow('td', 'empty', 'serverName', serverStatistics.serverName))
    tbody.appendChild(createTableRow('td', 'empty', 'cacheHit', serverStatistics.cacheHit))
    tbody.appendChild(createTableRow('td', 'empty', 'cacheTotal', serverStatistics.cacheTotal))
    tbody.appendChild(createTableRow('td', 'empty', 'cacheHitPercent', serverStatistics.cacheHitPercent + ' %'))
    tbody.appendChild(createTableRow('td', 'empty', 'noEventsRead', serverStatistics.noEventsRead))
    table.appendChild(tbody)
    return table
}


function getStatisticData() {
    var request = new XMLHttpRequest()
    request.open('GET', '/v1/serverStatistics', true)
    request.onload = function() {
        var data = JSON.parse(this.response)
        if (request.status >= 200 && request.status < 400) {
            const errorMessage = document.getElementById( 'errorMessage')
            errorMessage.textContent = ''
            container.innerHTML = ''
            const header1 = document.createElement('h3')
            header1.textContent = 'Server statistics'
            container.appendChild(header1)
            container.appendChild(createServerStatisticsTable(data))
            container.appendChild(document.createElement('br'))
            const header2 = document.createElement('h3')
            header2.textContent = 'Connection Statistics'
            container.appendChild(header2)
            container.appendChild(createConnectionStatisticsTable(data.connectionStatistics))
        } else {
            const errorMessage = document.getElementById( 'errorMessage')
            errorMessage.textContent = `Gah, it's not working!`
        }
    }
    request.send()
}

getStatisticData()

var timer = setInterval(myTimer, 1000) ;
function myTimer() {
    getStatisticData()
}