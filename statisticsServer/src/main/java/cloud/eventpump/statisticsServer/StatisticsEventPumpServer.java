package cloud.eventpump.statisticsServer;


import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.statisticsServer.connection.ServerStatisticsCalculator;

public class StatisticsEventPumpServer<Event> {

    private ServerStatisticsCalculator serverStatisticsCalculator = new ServerStatisticsCalculator();

    public StatisticsEventPumpServer(EventsPumpServer<Event> eventEventsPumpServer) {
        eventEventsPumpServer.setStatisticsServer(serverStatisticsCalculator);
    }

    public ServerStatistics getStatistics() {
        return serverStatisticsCalculator;
    }

}
