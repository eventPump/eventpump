package cloud.eventpump.server.connection;

import java.util.List;

public interface EPSConnectionRequires<Event> {

    void processEvents(List<Event> eventList) ;
}
