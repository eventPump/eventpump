package cloud.eventpump.statisticsServer;

import java.util.List;

public interface ServerStatistics {

    String getServerName();

    int getCacheHit();

    int getCacheTotal();

    int getNoEventsRead();

    List<? extends ConnectionStatistics> getConnectionStatistics();

    String getCacheHitPercent();
}
