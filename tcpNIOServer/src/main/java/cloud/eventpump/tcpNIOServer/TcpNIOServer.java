package cloud.eventpump.tcpNIOServer;

import cloud.eventpump.common.unexpected.CloseAppException;
import cloud.eventpump.common.unexpected.CloseConnectionException;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnection;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProcessor;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TcpNIOServer {

    private final static String TCP_NIO_SERVER_THREAD_NAME = "TcpNIOServer";

    private final TcpNIOConnectionProcessor connectionConsumer;
    private final UnexpectedHandler unexpectedHandler;

    private Selector selector;
    private ServerSocketChannel serverChannel;

    private List<TcpNIOConnection> tcpNIOConnections = new ArrayList<>();
    private volatile boolean sendInterruptedPending = false;
    private volatile boolean closePending = false;

    public TcpNIOServer(TcpNIOConnectionProcessor connectionProcessor, UnexpectedHandler unexpectedHandler) {
        this.connectionConsumer = connectionProcessor;
        this.unexpectedHandler = unexpectedHandler;
    }

    public void init(SocketAddress serverAddress) throws IOException {
        selector = Selector.open();
        serverChannel = ServerSocketChannel.open();
        serverChannel.bind(serverAddress);
        serverChannel.configureBlocking(false);
        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
    }

    public void start(SocketAddress serverAddress) throws IOException {
        init(serverAddress);
        Thread serverThread = new Thread(() -> {
            try {
                mainLoop();
            } catch (Exception ex) {
                unexpectedHandler.serverUnexpectedException(ex);
            }
        }, TCP_NIO_SERVER_THREAD_NAME);
        serverThread.start();
    }

    public void mainLoop() throws IOException {
        while (!closePending) {
            registerConnections();
            selector.select();
            processSelected();
            processInterrupted();
        }
        basicClose();
    }

    public void sendInterrupt() {
        sendInterruptedPending = true;
        selector.wakeup();
    }

    public void close() {
        closePending = true;
        selector.wakeup();
    }

    private void registerConnections() throws IOException {
        for (TcpNIOConnection nioConnection : tcpNIOConnections) {
            nioConnection.register(selector);
        }
    }

    private void processSelected() throws IOException {
        Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
        while (keyIterator.hasNext()) {
            SelectionKey key = keyIterator.next();
            if (key.isAcceptable()) {
                createNewConnection();
            } else {
                try {
                    if (key.isReadable()) {
                        if (!processRead(key)) {
                            closeClient(key);
                        }
                    }
                    if (key.isValid() && key.isWritable()) {
                        processWrite(key);
                    }
                } catch (CloseConnectionException ex) {
                    unexpectedHandler.serverCloseConnectionException(ex);
                    closeClient(key);
                } catch (CloseAppException ex) {
                    unexpectedHandler.serverCloseAppException(ex);
                    closePending = true;
                    break;
                }
            }
            keyIterator.remove();
        }
    }

    private void processInterrupted() throws IOException {
        if (sendInterruptedPending) {
            sendInterruptedPending = false;
            for (SelectionKey key : selector.keys()) {
                TcpNIOConnection tcpNIOConnection = tcpNIOConnectionForKey(key);
                if (tcpNIOConnection != null) {
                    try {
                        tcpNIOConnection.processCommit();
                    } catch (CloseConnectionException ex) {
                        unexpectedHandler.serverCloseConnectionException(ex);
                        closeClient(key);
                    } catch (CloseAppException ex) {
                        unexpectedHandler.serverCloseAppException(ex);
                        closePending = true;
                        break;
                    }
                }
            }
        }
    }

    private void createNewConnection() throws IOException {
        SocketChannel clientChannel = serverChannel.accept();
        clientChannel.configureBlocking(false);
        TcpNIOConnection tcpNIOConnection = new TcpNIOConnection(clientChannel, () -> selector.wakeup());
        TcpNIOConnectionRequires requires = connectionConsumer.createConnection(tcpNIOConnection);
        tcpNIOConnection.setTcpNIOConnectionRequires(requires);
        tcpNIOConnections.add(tcpNIOConnection);
    }

    private void processWrite(SelectionKey key) throws IOException {
        TcpNIOConnection tcpNIOConnection = tcpNIOConnectionForKey(key);
        tcpNIOConnection.processReadyToWrite();
    }

    private boolean processRead(SelectionKey key) throws IOException {
        TcpNIOConnection tcpNIOConnection = tcpNIOConnectionForKey(key);
        return tcpNIOConnection.processReadyToRead();
    }

    private void closeClient(SelectionKey key) throws IOException {
        TcpNIOConnection tcpNIOConnection = tcpNIOConnectionForKey(key);
        tcpNIOConnection.close();
        tcpNIOConnections.remove(tcpNIOConnection);
        connectionConsumer.closeConnection(tcpNIOConnection.getTcpNIOConnectionRequires());
    }

    private TcpNIOConnection tcpNIOConnectionForKey(SelectionKey key) {
        return (TcpNIOConnection) key.attachment();
    }

    private void basicClose() throws IOException {
        selector.close();
        serverChannel.close();
    }

}
