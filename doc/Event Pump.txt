EventPump 
Event Pump library for distributed event processing. 

EventPump main features
* decentralized (of course distributed) � works as part of event exchanging application/service, no central point of failure, no centralized configuration 
* reliable/transactional � has been designed in such a way that easily/naturally accommodate transaction handling 
* exactly once semantics provided (with transaction handling)
* fast � based on TCP, without overhead for HTTP/HTTPS communication 
* backpressure/catchup support
* multi-database � not related to any database, not aware about database at all, small adapter code have to be written, based on that Events can be exchanged between different types of databases (relational, document, graph)
* no need to provide contingency/emergency algorithms in case of e.g. network partitions � after successful connection Client applies all the Events that have been issued on Server
* light weight � it is a library not a framework, light jars, no additional dependencies  
* monitoring � Api, RESTFul Api, tiny http statistic server

How EventPump works
Important assumption: Events on Sever have monotonically growing ids (gaps are allowed). The same ids have to be stored on Client side to track events that have been applied.  
Simplified algorithm: 
1. Client: Client starts, EventsApplier.findLastAppliedDbId() is called. The value represent last applied by client Event Id (lastAppliedDbId). 
2. Client-Server:  lastAppliedDbId is send to Server. 
3. Server: Server executes EventsProviderImpl.provideEvents(lastAppliedDbId) if there are Events with bigger dbId(s) they are read. Function reads at once as many Events as is allowed by buffer size (configurable default 1000).  
4. Server-Clinet: Marshalled Events are send to Clinet
5. Clinet: EventsApplier.applyEvents() is called, as its result function returns Id of last applied Event
6. Client-Server:  lastAppliedDbId is send to Server (ACK). 
7. Server:  If there is fee space in buffer EventsProviderImpl.provideEvents(lastAppliedDbId) is called immediately, if there are Events with bigger dbId(s) they are read, the execution goes to (4). It there are no more events processing stops waiting for successful commit to occur (8). 
8. Server: Some Business Logic is executed which results in changes in content of DB-Server and storing some Domain Events in DB-Server (in one transaction)
9. Server: After successful transaction commit, commit handler registered in DB-Server executes EventPumpServer.commit() as a result processing goes to (3)

Remarks: 
(A) If commit occurs after sending data to Clinet and before receiving ACK and there is free space in sending buffer function EventsProviderImpl.provideEvents(lastAppliedDbId) is called new Events are read and send immediately. 
       There are two triggers that can start of reading Events: 
1. Commit on Server 
2. Information send by Clinet (ACK) that Events have been applied on Client side. 
   In both case Events are read up to capacity of sending buffer. 
(B) There is no need to implement any contingency/emergency algorithms in case of e.g. network partitions between Client and Server. In such situation after connection is established all events stored in the mean time on the Server are automatically send to Client. 

