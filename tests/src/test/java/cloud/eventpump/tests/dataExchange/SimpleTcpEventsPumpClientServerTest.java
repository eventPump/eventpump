package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.ClientFakeDB;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class SimpleTcpEventsPumpClientServerTest {

    @Test
    public void smallDataAmountExchangeTest() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eServer, new TcpServerConfig<>(new JacksonMarshaller<>()));
        serverFakeDB.setCommit(eServer::commit);
        tcpEventsPumpServer.start();

        ClientFakeDB clientFakeDB = new ClientFakeDB();
        clientFakeDB.setAutoAck(true);
        TcpClientConfig<TEvent> tcpClientConfig = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        TcpEventsPumpClient<TEvent> tcpEventsPumpClient = new TcpEventsPumpClient<>(clientFakeDB, tcpClientConfig);
        tcpEventsPumpClient.start();

        Thread.sleep(10);
        serverFakeDB.insert("a", "b", "c", "d", "e", "f", "g");
        Thread.sleep(10);
        serverFakeDB.insert("h", "i", "j");
        Thread.sleep(100);

        assertEquals(serverFakeDB.strContent(), clientFakeDB.strContent());

        tcpEventsPumpServer.close();
        tcpEventsPumpClient.close();
        Thread.sleep(100);
    }

    @Test
    public void lotsOfDataExchangeTest() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eServer, new TcpServerConfig<>(new JacksonMarshaller<>()));
        serverFakeDB.setCommit(eServer::commit);
        tcpEventsPumpServer.start();


        ClientFakeDB clientFakeDB = new ClientFakeDB();
        clientFakeDB.setAutoAck(true);
        TcpClientConfig<TEvent> tcpClientConfig = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        TcpEventsPumpClient<TEvent> tcpEventsPumpClient = new TcpEventsPumpClient<>(clientFakeDB, tcpClientConfig);
        tcpEventsPumpClient.start();

        Thread.sleep(10);
        serverFakeDB.insert(generateStrings("s", 50));
        Thread.sleep(10);
        serverFakeDB.insert(generateStrings("p", 50));
        Thread.sleep(10);
        serverFakeDB.insert(generateStrings("w", 50));
        Thread.sleep(300);

        assertEquals(serverFakeDB.strContent(), clientFakeDB.strContent());

        tcpEventsPumpServer.close();
        tcpEventsPumpClient.close();
        Thread.sleep(100);
    }


    @Test
    public void clientClosesConnection() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eServer, new TcpServerConfig<>(new JacksonMarshaller<>()));
        serverFakeDB.setCommit(eServer::commit);
        tcpEventsPumpServer.start();


        ClientFakeDB cDB1 = new ClientFakeDB();
        cDB1.setAutoAck(true);
        TcpClientConfig<TEvent> c1Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(cDB1, c1Configuration);
        c1.start();

        ClientFakeDB cDB2 = new ClientFakeDB();
        cDB2.setAutoAck(true);
        TcpClientConfig<TEvent> c2Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        TcpEventsPumpClient<TEvent> c2 = new TcpEventsPumpClient<>(cDB2, c2Configuration);
        c2.start();


        Thread.sleep(50);
        serverFakeDB.insert(generateStrings("a", 50));
        c1.close();

        Thread.sleep(50);
        serverFakeDB.insert(generateStrings("b", 50));

        Thread.sleep(200);
        assertEquals(serverFakeDB.strContent(), cDB2.strContent());

        c2.close();
        tcpEventsPumpServer.close();
        Thread.sleep(100);
    }


    @Test
    public void serverClosesConnection() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eServer, new TcpServerConfig<>(new JacksonMarshaller<>()));
        serverFakeDB.setCommit(eServer::commit);
        tcpEventsPumpServer.start();


        ClientFakeDB cDB1 = new ClientFakeDB();
        cDB1.setAutoAck(true);
        TcpClientConfig<TEvent> c1Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(cDB1, c1Configuration);
        c1.start();

        ClientFakeDB cDB2 = new ClientFakeDB();
        cDB2.setAutoAck(true);
        TcpClientConfig<TEvent> c2Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        TcpEventsPumpClient<TEvent> c2 = new TcpEventsPumpClient<>(cDB2, c2Configuration);
        c2.start();

        Thread.sleep(10);
        serverFakeDB.insert(generateStrings("s", 50));
        Thread.sleep(10);
        tcpEventsPumpServer.close();


        c1.close();
        c2.close();
        Thread.sleep(300);
    }


    private String[] generateStrings(String prefix, int count) {
        String[] result = new String[count];
        for (int i = 0; i < count; i++) {
            result[i] = prefix + "-" + i;
        }
        return result;
    }
}
