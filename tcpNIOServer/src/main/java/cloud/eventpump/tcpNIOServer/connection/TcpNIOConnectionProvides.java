package cloud.eventpump.tcpNIOServer.connection;

import java.nio.ByteBuffer;
import java.util.List;

public interface TcpNIOConnectionProvides {

    void send(List<ByteBuffer> byteBuffers) ;

}
