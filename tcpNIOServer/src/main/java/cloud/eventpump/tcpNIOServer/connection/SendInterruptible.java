package cloud.eventpump.tcpNIOServer.connection;

public interface SendInterruptible {

    void wakeUpForSend() ;

}
