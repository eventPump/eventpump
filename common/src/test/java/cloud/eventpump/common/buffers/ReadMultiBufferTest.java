package cloud.eventpump.common.buffers;

import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;

public class ReadMultiBufferTest {

    @Test
    public void getBytes1Test() {
        ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        readMultiBuffer.append(bb("ab"));
        readMultiBuffer.append(bb("c"));
        readMultiBuffer.append(bb("def"));

        assertEquals("a", new String(readMultiBuffer.getBytes(1)));
        assertEquals("bcd", new String(readMultiBuffer.getBytes(3)));
        assertEquals("ef", new String(readMultiBuffer.getBytes(2)));
    }


    @Test
    public void getBytes2Test() {
        ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        readMultiBuffer.append(bb("ab"));
        readMultiBuffer.append(bb("cdefg"));
        readMultiBuffer.append(bb("hi"));

        assertEquals("abc", new String(readMultiBuffer.getBytes(3)));
        assertEquals("de", new String(readMultiBuffer.getBytes(2)));
        assertEquals("fg", new String(readMultiBuffer.getBytes(2)));
        assertEquals("hi", new String(readMultiBuffer.getBytes(2)));
    }

    @Test
    public void getBytes3Test() {
        String result = "abcdefghi";
        for (int i = 0; i < result.length(); i++) {
            for (int j = i; j < result.length(); j++) {
                for (int k = j; k < result.length(); k++) {
                    ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
                    readMultiBuffer.append(bb("ab"));
                    readMultiBuffer.append(bb("cdefg"));
                    readMultiBuffer.append(bb("hi"));

                    String s1 = new String(readMultiBuffer.getBytes(i - 0));
                    String s2 = new String(readMultiBuffer.getBytes(j - i));
                    String s3 = new String(readMultiBuffer.getBytes(k - j));
                    String s4 = new String(readMultiBuffer.getBytes(result.length() - k));

                    // System.out.println( s1 + "." + s2 + "." + s3 + "." + s4);
                    assertEquals(result, s1 + s2 + s3 + s4);
                }
            }
        }
    }

    @Test
    public void getBytes4Test() {
        ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        readMultiBuffer.append(bb("ab"));
        readMultiBuffer.append(bb("cdefg"));

        assertEquals("abc", new String(readMultiBuffer.getBytes(3)));
        assertEquals("de", new String(readMultiBuffer.getBytes(2)));

        readMultiBuffer.append(bb("hi"));

        assertEquals("fgh", new String(readMultiBuffer.getBytes(3)));
        assertEquals("i", new String(readMultiBuffer.getBytes(1)));
    }

    @Test
    public void getSizeTest() {
        ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        assertEquals( 0, readMultiBuffer.getSize());
        readMultiBuffer.append(bb("ab"));
        assertEquals( 2, readMultiBuffer.getSize());
        readMultiBuffer.append(bb("cdefg"));
        assertEquals( 7, readMultiBuffer.getSize());
        readMultiBuffer.getBytes(3);
        assertEquals( 4, readMultiBuffer.getSize());
        readMultiBuffer.append(bb("hi"));
        assertEquals( 6, readMultiBuffer.getSize());
        readMultiBuffer.getBytes(3);
        assertEquals( 3, readMultiBuffer.getSize());
        readMultiBuffer.getBytes(3);
        assertEquals( 0, readMultiBuffer.getSize());
    }

    private ByteBuffer bb(String str) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        byteBuffer.put(str.getBytes());
        byteBuffer.flip() ;
        return byteBuffer;
    }


}