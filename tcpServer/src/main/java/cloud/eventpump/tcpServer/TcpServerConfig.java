package cloud.eventpump.tcpServer;

import cloud.eventpump.common.marshaller.EventMarshaller;
import cloud.eventpump.common.unexpected.UnexpectedHandler;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

public class TcpServerConfig<Event> {

    private SocketAddress serverAddress = new InetSocketAddress(9875);
    private EventMarshaller<Event> eventMarshaller;
    private UnexpectedHandler unexpectedHandler = new UnexpectedHandler();

    private int writeBufferSize = 1024 * 4;

    public TcpServerConfig(EventMarshaller<Event> eventMarshaller) {
        this.eventMarshaller = eventMarshaller ;
    }

    public SocketAddress getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(SocketAddress serverAddress) {
        this.serverAddress = serverAddress;
    }

    public EventMarshaller<Event> getEventMarshaller() {
        return eventMarshaller;
    }

    public void setEventMarshaller(EventMarshaller<Event> eventMarshaller) {
        this.eventMarshaller = eventMarshaller;
    }

    public UnexpectedHandler getUnexpectedHandler() {
        return unexpectedHandler;
    }

    public void setUnexpectedHandler(UnexpectedHandler unexpectedHandler) {
        this.unexpectedHandler = unexpectedHandler;
    }

    public int getWriteBufferSize() {
        return writeBufferSize;
    }

    public void setWriteBufferSize(int writeBufferSize) {
        this.writeBufferSize = writeBufferSize;
    }

    @Override
    public String toString() {
        return "TcpServerConfig{" +
                "serverAddress=" + serverAddress +
                ", eventMarshaller=" + eventMarshaller +
                ", unexpectedHandler=" + unexpectedHandler +
                ", writeBufferSize=" + writeBufferSize +
                '}';
    }
}
