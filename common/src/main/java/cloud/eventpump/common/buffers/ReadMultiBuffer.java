package cloud.eventpump.common.buffers;

import cloud.eventpump.common.unexpected.CloseAppException;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToInt;
import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToLong;


public class ReadMultiBuffer {

    private LinkedList<ByteBuffer> buffers = new LinkedList<>();

    private int totalRemainingSize;

    public void append(ByteBuffer byteBuffer) {
        totalRemainingSize += byteBuffer.limit();
        buffers.add(byteBuffer);
    }

    public int getSize() {
        return totalRemainingSize;
    }

    public byte[] getBytes(int count) {
        byte[] result = new byte[count];
        for (int writePos = 0; writePos < count; ) {
            ByteBuffer firstBuffer = null ;
            try {
                firstBuffer = buffers.getFirst();
            } catch (NoSuchElementException ex) {
                throw new CloseAppException("Read multi buffer empty ", ex);
            }
            int copyCount = Math.min(firstBuffer.remaining(), count - writePos);
            firstBuffer.get(result, writePos, copyCount);
            writePos += copyCount;
            if (firstBuffer.remaining() == 0) {
                buffers.removeFirst();
            }
        }
        totalRemainingSize -= count;
        return result;
    }


}
