package cloud.eventpump.tcpClient.connection.marshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.frame.OutputFrameBuilder;
import cloud.eventpump.common.marshaller.Marshaller;

public class AckMarshaller extends Marshaller {

    public AckMarshaller(OutputFrameBuilder outputFrameBuilder) {
        super(outputFrameBuilder);
    }

    public void ack(long ack) {
        outputFrameBuilder.startFrame(FrameNo.ACK_v0100);
        outputFrameBuilder.putLong(ack);
        outputFrameBuilder.finishFrame();
    }
}
