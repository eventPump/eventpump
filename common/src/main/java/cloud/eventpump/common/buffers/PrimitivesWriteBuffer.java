package cloud.eventpump.common.buffers;

public interface PrimitivesWriteBuffer {

    void putInt( int value ) ;
    void putLong( long value ) ;
    void putString( String value ) ;
    void putBytes(byte[] value ) ;
}
