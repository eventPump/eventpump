package cloud.eventpump.server.connection;


import cloud.eventpump.common.UpdateConnectionStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class EPSConnectionProcessor<Event> implements EPSConnectionProvides {

    private final Logger logger = LoggerFactory.getLogger(EPSConnectionProvides.class);

    private final AckProcessor<Event> ackProcessor;
    private final ReadController<Event> readController;
    private final EPSConnectionRequires<Event> epDataConnectionRequires;
    private final UpdateConnectionStatistics updateConnectionStatistics;

    public EPSConnectionProcessor(
            AckProcessor<Event> ackProcessor, ReadController<Event> readController,
            EPSConnectionRequires<Event> epDataConnectionRequires, UpdateConnectionStatistics updateConnectionStatistics) {
        this.ackProcessor = ackProcessor;
        this.readController = readController;
        this.epDataConnectionRequires = epDataConnectionRequires;
        this.updateConnectionStatistics = updateConnectionStatistics;
    }

    public UpdateConnectionStatistics getUpdateConnectionStatistics() {
        return updateConnectionStatistics;
    }

    @Override
    public void setStartDBId(long dbId) {
        ackProcessor.setLastAppliedDBId(dbId);
        tryProcessEvents();
    }

    @Override
    public void processCommit() {
        readController.clear();
        tryProcessEvents();
    }

    @Override
    public void applySentBytesCount(int sentBytesCount) {
        updateConnectionStatistics.applySentBytesCount(sentBytesCount);
    }

    @Override
    public void applyReceivedBytesCount(int receivedBytesCount) {
        updateConnectionStatistics.applyReceivedBytesCount(receivedBytesCount);
    }

    @Override
    public void setClientName(String clientName) {
        updateConnectionStatistics.setClientName(clientName);
        logger.info("Connected clientName="+ clientName);
    }

    @Override
    public void ack(long dbId) {
        ackProcessor.ack(dbId);
        tryProcessEvents();
    }

    private void tryProcessEvents() {
        List<Event> events = readController.provideEvents(
                ackProcessor.getLastAppliedDBId(),
                ackProcessor.getPossibleReadCount());
        if (!events.isEmpty()) {
            ackProcessor.applyInProgressEvents(events);
            epDataConnectionRequires.processEvents(events);
        }
        updateConnectionStatistics.setClientSynchronized(ackProcessor.isClientSynchronized()) ;
    }

}
