package cloud.eventpump.statisticsServer;

public interface ConnectionStatistics {

    String getClientName();

    long getLastReceivedAckDbId();

    long getLastReceivedAckUtcTs();

    long getLastReceivedAckEventUtcTs();

    long getClientLag();

    boolean isClientSynchronized();

    int getTotalSentBytesCount();

    int getTotalReceivedBytesCount();

    int getTotalReadEventsCount();

    int getTotalAckEventsCount();
}
