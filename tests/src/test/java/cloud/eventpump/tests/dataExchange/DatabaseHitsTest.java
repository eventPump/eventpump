package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.localClient.LocalClientConfig;
import cloud.eventpump.localClient.LocalEventsPumpClient;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.tests.testTools.ClientFakeDB;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class DatabaseHitsTest {

    private ServerFakeDB serverFakeDB;
    private EventsPumpServer<TEvent> eventEventsPumpServer;

    private ClientFakeDB clientFakeDB1 = new ClientFakeDB();
    private LocalEventsPumpClient<TEvent> localEventsPumpClient1;

    private ClientFakeDB clientFakeDB2 = new ClientFakeDB();
    private LocalEventsPumpClient<TEvent> localEventsPumpClient2;

    @Before
    public void before() throws InterruptedException {
        serverFakeDB = new ServerFakeDB();
        eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        serverFakeDB.setCommit(eventEventsPumpServer::commit);

        clientFakeDB1 = new ClientFakeDB();
        localEventsPumpClient1 = new LocalEventsPumpClient<>(clientFakeDB1, eventEventsPumpServer, new LocalClientConfig());
        localEventsPumpClient1.start();

        clientFakeDB2 = new ClientFakeDB();
        localEventsPumpClient2 = new LocalEventsPumpClient<>(clientFakeDB2, eventEventsPumpServer, new LocalClientConfig());
        localEventsPumpClient2.start();

        Thread.sleep( 20 );

        // The first hit to DB caused by setting startDBId
        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        assertEquals(1, lastReads.size());
        serverFakeDB.clearLastReads();
    }


    @Test
    public void cacheHitTest() throws InterruptedException {
        serverFakeDB.insert("v1");
        Thread.sleep(20);

        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        assertEquals(1, lastReads.size());

        assertEquals(serverFakeDB.strContent(), clientFakeDB1.strContent());
        assertEquals(serverFakeDB.strContent(), clientFakeDB2.strContent());
    }

    @Test
    public void nothingToReadHitTest() throws InterruptedException {
        eventEventsPumpServer.commit();
        Thread.sleep(20);

        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        assertEquals(1, lastReads.size());

        assertEquals(serverFakeDB.strContent(), clientFakeDB1.strContent());
        assertEquals(serverFakeDB.strContent(), clientFakeDB2.strContent());
    }

    @Test
    public void nothingToReadAndThanReadHitTest() throws InterruptedException {
        eventEventsPumpServer.commit();
        Thread.sleep(20);

        List<List<TEvent>> lastReads = serverFakeDB.getLastReads();
        assertEquals(1, lastReads.size());

        serverFakeDB.insert("v1");
        Thread.sleep(20);

        lastReads = serverFakeDB.getLastReads();
        assertEquals(2, lastReads.size());

        assertEquals(serverFakeDB.strContent(), clientFakeDB1.strContent());
        assertEquals(serverFakeDB.strContent(), clientFakeDB2.strContent());
    }


}
