package cloud.eventpump.tcpClient.connection.unmarshaller;

import java.util.List;

public interface EventConsumer<Event> {
    void precessEvents(List<Event> events) ;
}
