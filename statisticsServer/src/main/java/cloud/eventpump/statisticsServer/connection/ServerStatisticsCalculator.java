package cloud.eventpump.statisticsServer.connection;

import cloud.eventpump.common.UpdateConnectionStatistics;
import cloud.eventpump.common.UpdateServerStatistics;
import cloud.eventpump.statisticsServer.ConnectionStatistics;
import cloud.eventpump.statisticsServer.ServerStatistics;

import java.util.ArrayList;
import java.util.List;

public class ServerStatisticsCalculator implements UpdateServerStatistics, ServerStatistics {

    private final List<ConnectionStatisticCalculator> connectionStatisticCalculators = new ArrayList<>();

    private String serverName = "";
    private int cacheHit = 0;
    private int cacheTotal = 0;
    private int noEventsRead = 0;

    @Override
    public UpdateConnectionStatistics createConnection() {
        ConnectionStatisticCalculator connectionStatisticCalculator = new ConnectionStatisticCalculator();
        connectionStatisticCalculators.add(connectionStatisticCalculator);
        return connectionStatisticCalculator;
    }

    @Override
    public void closeConnection(UpdateConnectionStatistics updateConnectionStatistics) {
        connectionStatisticCalculators.remove((ConnectionStatisticCalculator) updateConnectionStatistics);
    }

    @Override
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    @Override
    public void apply(HitType hitType) {
        switch (hitType) {
            case EventsFoundInCache:
                cacheHit++;
                cacheTotal++;
                break;
            case EventsNotFoundInCache:
                cacheTotal++;
                break;
            case NoEventsProvided:
                noEventsRead++;
                break;
        }
    }

    // Server statistics
    @Override
    public String getServerName() {
        return serverName;
    }

    @Override
    public int getCacheHit() {
        return cacheHit;
    }

    @Override
    public int getCacheTotal() {
        return cacheTotal;
    }

    @Override
    public int getNoEventsRead() {
        return noEventsRead;
    }

    @Override
    public List<? extends ConnectionStatistics> getConnectionStatistics() {
        return connectionStatisticCalculators;
    }

    @Override
    public String getCacheHitPercent() {
        double hitPercent = cacheTotal > 0 ? (100.00 * cacheHit) / cacheTotal : 0.0;
        return String.format("%.2f", hitPercent);
    }
}
