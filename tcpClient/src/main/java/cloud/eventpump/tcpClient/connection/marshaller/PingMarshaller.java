package cloud.eventpump.tcpClient.connection.marshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.frame.OutputFrameBuilder;
import cloud.eventpump.common.marshaller.Marshaller;

public class PingMarshaller extends Marshaller {

    public PingMarshaller(OutputFrameBuilder outputFrameBuilder) {
        super(outputFrameBuilder);
    }

    public void ping() {
        outputFrameBuilder.startFrame(FrameNo.PING_v0100);
        outputFrameBuilder.finishFrame();
    }
}
