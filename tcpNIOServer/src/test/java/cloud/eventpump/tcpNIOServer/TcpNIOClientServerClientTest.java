package cloud.eventpump.tcpNIOServer;


import cloud.eventpump.common.buffers.PrimitiveConversions;
import cloud.eventpump.common.buffers.ReadMultiBuffer;
import cloud.eventpump.common.buffers.WriteMultiBuffer;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProcessor;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionRequires;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static cloud.eventpump.common.buffers.PrimitiveConversions.bytesToLong;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TcpNIOClientServerClientTest implements TcpNIOConnectionProcessor {

    private int closeConnectionCount = 0;

    private TcpNIOServer server ;
    private TcpNIOClient client ;

    private ServerConnection serverConnection ;
    private ClientConnection clientConnection ;

    @Before
    public void before() throws IOException {
        closeConnectionCount = 0 ;
        server = new TcpNIOServer(this, new UnexpectedHandler());
        server.start(new InetSocketAddress(9872));

        client = new TcpNIOClient();
        clientConnection = new ClientConnection();
        TcpNIOConnectionProvides clientProvides = client.init(new InetSocketAddress(9872), null, clientConnection);
        clientConnection.setTcpNIOConnectionProvides(clientProvides);
        Thread thread = new Thread( () -> {
            try {
                client.mainLoop(0) ;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } ) ;
        thread.start();
    }

    @Test
    public void bulkClientServerClientExchange() throws IOException, InterruptedException {
        int waitForServerCount = 0;
        int allowedOnTheFly = 100 * 1024;
        for (int i = 0; i < 1024; i++) {
            clientConnection.send(1024);
            while (clientConnection.getReceivedCounter() + allowedOnTheFly < i * 1024) {
                Thread.sleep(1);
                waitForServerCount++;
            }
        }

        while (clientConnection.getReceivedCounter() < 1024 * 1024) {
            Thread.sleep(100);
        }

        client.close();
        Thread.sleep(10);
        server.close();

        assertTrue(waitForServerCount > 5);
        assertEquals(1, closeConnectionCount);
    }

    @Test
    public void unexpectedClientClose() throws IOException, InterruptedException {
        int allowedOnTheFly = 100 * 1024;
        for (int i = 0; i < 1024; i++) {
            clientConnection.send(1024);
            if (clientConnection.getReceivedCounter() + allowedOnTheFly < i * 1024) {
                client.close();
            }
        }

        Thread.sleep( 3 );
        server.close();
        assertEquals(1, closeConnectionCount);
    }

    @Test
    public void unexpectedServerClose() throws IOException, InterruptedException {
        int allowedOnTheFly = 100 * 1024;
        for (int i = 0; i < 1024; i++) {
            clientConnection.send(1024);
            if (clientConnection.getReceivedCounter() + allowedOnTheFly < i * 1024) {
                server.close();
            }
        }

        Thread.sleep( 3 );
        client.close();
    }

    @Override
    public TcpNIOConnectionRequires createConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
        return serverConnection = new ServerConnection(tcpNIOConnectionProvides);
    }

    @Override
    public void closeConnection(TcpNIOConnectionRequires tcpNIOConnectionRequires) {
        closeConnectionCount++;
    }


    class ServerConnection implements TcpNIOConnectionRequires {

        private WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(10 * 1024);
        private int sentCounter = 1 ;

        private final TcpNIOConnectionProvides tcpNIOConnectionProvides;

        public ServerConnection(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
            this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        }

        public void send(int count) {
            for (int i = 0; i < count; i++) {
                writeMultiBuffer.putBytes(PrimitiveConversions.longToBytes(sentCounter++));
            }
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }

        @Override
        public void received(ByteBuffer byteBuffer) {
            tcpNIOConnectionProvides.send(Arrays.asList(byteBuffer));
        }

        @Override
        public void interrupted() {
        }
    }

    class ClientConnection implements TcpNIOConnectionRequires {

        private TcpNIOConnectionProvides tcpNIOConnectionProvides;

        private ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        private WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(10 * 1024);

        private int sentCounter = 1;
        private volatile int receivedCounter = 1;

        public void setTcpNIOConnectionProvides(TcpNIOConnectionProvides tcpNIOConnectionProvides) {
            this.tcpNIOConnectionProvides = tcpNIOConnectionProvides;
        }

        public int getReceivedCounter() {
            return receivedCounter;
        }

        @Override
        public void received(ByteBuffer byteBuffer) {
            readMultiBuffer.append(byteBuffer);
            while (readMultiBuffer.getSize() >= 8) {
                long receivedValue = bytesToLong(readMultiBuffer.getBytes(8));
                assertEquals(receivedCounter, receivedValue);
                receivedCounter++;
            }
        }

        @Override
        public void interrupted() {
        }

        public void send(int count) {
            for (int i = 0; i < count; i++) {
                writeMultiBuffer.putBytes(PrimitiveConversions.longToBytes(sentCounter++));
            }
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }
    }
}