package cloud.eventpump.server.connection;

import cloud.eventpump.server.EventsProvider;

import java.util.Collections;
import java.util.List;

public class ReadController<Event> {

    private final EventsProvider<Event> eventsProvider;
    private final int eventsReadThreshold;

    private boolean nothingToRead = false;

    public ReadController(EventsProvider<Event> eventsProvider, int eventsReadThreshold) {
        this.eventsProvider = eventsProvider;
        this.eventsReadThreshold = eventsReadThreshold;
    }

    public List<Event> provideEvents(long lastAppliedDBId, int maxCount) {
        if (nothingToRead || maxCount < eventsReadThreshold || lastAppliedDBId == -1) {
            return Collections.emptyList();
        }
        List<Event> events = eventsProvider.provideEvents(lastAppliedDBId, maxCount);
        nothingToRead = events.size() < maxCount;
        return events;
    }

    public void clear() {
        nothingToRead = false;
    }
}

