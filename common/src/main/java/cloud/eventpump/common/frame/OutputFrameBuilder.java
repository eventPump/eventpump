package cloud.eventpump.common.frame;


import cloud.eventpump.common.buffers.PrimitivesWriteBuffer;
import cloud.eventpump.common.buffers.WriteMultiBuffer;

import static cloud.eventpump.common.buffers.PrimitiveConversions.intToBytes;
import static cloud.eventpump.common.buffers.PrimitiveConversions.longToBytes;

public class OutputFrameBuilder implements PrimitivesWriteBuffer {

    private final WriteMultiBuffer writeMultiBuffer;
    private WriteMultiBuffer.PlaceholderInfo placeholderInfo;
    private int startFrameBufferSize = -1;

    public OutputFrameBuilder(WriteMultiBuffer writeMultiBuffer) {
        this.writeMultiBuffer = writeMultiBuffer;
    }

    public void startFrame(int frameNo) {
        // TODO startFrameBufferSize == -1, placeholder == null
        placeholderInfo = writeMultiBuffer.reservePlaceholder(4);
        startFrameBufferSize = writeMultiBuffer.getTotalSize();
        putInt(frameNo);
    }

    public void putInt(int value) {
        writeMultiBuffer.putBytes(intToBytes(value));
    }

    public void putLong(long value) {
        writeMultiBuffer.putBytes(longToBytes(value));
    }

    public void putString(String value) {
        byte[] bytes = value.getBytes();
        putInt(bytes.length);
        writeMultiBuffer.putBytes(bytes);
    }

    @Override
    public void putBytes(byte[] value) {
        putInt(value.length);
        writeMultiBuffer.putBytes(value);
    }

    public void finishFrame() {
        // TODO startFrameBufferSize != -1
        int frameLength = writeMultiBuffer.getTotalSize() - startFrameBufferSize;
        writeMultiBuffer.overwritePlaceholder(placeholderInfo, intToBytes(frameLength));
        placeholderInfo = null;
        startFrameBufferSize = -1;
    }

}
