package cloud.eventpump.server;

import java.util.List;

public interface EventsProvider<Event> {

    List<Event> provideEvents(long dbId, int maxEventCount);

}
