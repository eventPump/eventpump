package cloud.eventpump.server;


import cloud.eventpump.server.connection.EPSConnectionProvides;
import cloud.eventpump.server.connection.EPSConnectionRequires;

public interface EPSClientHandle<Event> {

    EPSConnectionProvides createConnection(EPSConnectionRequires<Event> eventEPDataConnectionRequires);

    void closeConnection(EPSConnectionProvides epsConnectionProvides) ;

    void appendCommitEventConsumer(Runnable commitEventConsumer);
}
