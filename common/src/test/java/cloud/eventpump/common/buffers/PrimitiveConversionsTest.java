package cloud.eventpump.common.buffers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrimitiveConversionsTest {

    @Test
    public void integerConversionTest() {
        checkIntegerConversion(0);
        checkIntegerConversion(-1);
        checkIntegerConversion(0x04030201);
        checkIntegerConversion(0xF0F1F2F3);
        checkIntegerConversion(Integer.MAX_VALUE);
        checkIntegerConversion(Integer.MIN_VALUE);
    }

    @Test
    public void longConversionTest() {
        checkIntegerConversion(0L);
        checkIntegerConversion(-1L);
        checkIntegerConversion(0x0807060504030201L);
        checkIntegerConversion(0xF0F1F2F3F4F5F6F7L);
        checkIntegerConversion(Long.MAX_VALUE);
        checkIntegerConversion(Long.MIN_VALUE);
    }

    private void checkIntegerConversion(int value) {
        byte[] bytes = PrimitiveConversions.intToBytes(value);
        int actual = PrimitiveConversions.bytesToInt(bytes);
        assertEquals(value, actual);
    }

    private void checkIntegerConversion(long value) {
        byte[] bytes = PrimitiveConversions.longToBytes(value);
        long actual = PrimitiveConversions.bytesToLong(bytes);
        assertEquals(value, actual);
    }

}