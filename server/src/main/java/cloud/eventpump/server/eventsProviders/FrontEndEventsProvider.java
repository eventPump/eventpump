package cloud.eventpump.server.eventsProviders;


import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.EventsProvider;

import java.util.Collections;
import java.util.List;

public class FrontEndEventsProvider<Event> implements EventsProvider<Event> {

    private final EventsProvider<Event> eventsProvider;
    private final UnexpectedHandler unexpectedHandler;

    public static <Event> FrontEndEventsProvider<Event> create(EventsProvider<Event> eventsProvider, UnexpectedHandler unexpectedHandler) {
        return new FrontEndEventsProvider<>(eventsProvider, unexpectedHandler);
    }

    public FrontEndEventsProvider(EventsProvider<Event> eventsProvider, UnexpectedHandler unexpectedHandler) {
        this.eventsProvider = eventsProvider;
        this.unexpectedHandler = unexpectedHandler;
    }

    @Override
    public List<Event> provideEvents(long dbId, int maxEventCount) {
        List<Event> events = safeProvideEvents(dbId, maxEventCount);
        if (events == null) {
            return Collections.emptyList();
        }
        if (events.size() > maxEventCount) {
            unexpectedHandler.tooManyEventsProvided(maxEventCount, events.size());
            events = events.subList(0, maxEventCount);
        }
        return events;
    }

    private List<Event> safeProvideEvents(long dbId, int maxEventCount) {
        try {
            return eventsProvider.provideEvents(dbId, maxEventCount);
        } catch ( Exception ex ) {
            unexpectedHandler.exceptionCaughtDuringProvideEvents( ex ) ;
            return null ;
        }
    }

}
