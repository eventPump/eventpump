package cloud.eventpump.server.connection;

public interface EPSConnectionProvides {

    void setStartDBId(long dbId);

    void ack(long dbId);

    void processCommit();

    void applySentBytesCount(int sentBytesCount);

    void applyReceivedBytesCount(int receivedBytesCount);

    void setClientName(String clientName);

}
