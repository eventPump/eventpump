package cloud.eventpump.common.unexpected;

public class CloseConnectionNIOException extends CloseConnectionException {
    public CloseConnectionNIOException() {
    }

    public CloseConnectionNIOException(String message) {
        super(message);
    }

    public CloseConnectionNIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public CloseConnectionNIOException(Throwable cause) {
        super(cause);
    }

    public CloseConnectionNIOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
