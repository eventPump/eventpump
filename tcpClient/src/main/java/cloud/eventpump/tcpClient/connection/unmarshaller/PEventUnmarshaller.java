package cloud.eventpump.tcpClient.connection.unmarshaller;


import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.EventUnmarshaller;
import cloud.eventpump.common.marshaller.Unmarshaller;
import cloud.eventpump.common.unexpected.CloseAppException;

import java.util.ArrayList;
import java.util.List;

public class PEventUnmarshaller<Event> implements Unmarshaller {

    private List<Event> pEvents = new ArrayList<>();

    private final EventUnmarshaller<Event> eventEventUnmarshaller;
    private final EventConsumer<Event> eventEventConsumer;

    public PEventUnmarshaller(EventUnmarshaller<Event> eventEventUnmarshaller, EventConsumer<Event> eventEventConsumer) {
        this.eventEventUnmarshaller = eventEventUnmarshaller;
        this.eventEventConsumer = eventEventConsumer;
    }

    @Override
    public void unmarshall(PrimitivesReadBuffer primitivesReadBuffer) {
        try {
            Event event = eventEventUnmarshaller.unmarshall(primitivesReadBuffer);
            if ( event == null ) {
                throw new CloseAppException("Unmarshaller returned null value") ;
            }
            pEvents.add(event);
        } catch (Exception ex ) {
            throw new CloseAppException("Exception during unmarshalling ", ex) ;
        }
    }

    public void flush() {
        if (!pEvents.isEmpty()) {
            eventEventConsumer.precessEvents(pEvents);
            pEvents = new ArrayList<>();
        }
    }

}
