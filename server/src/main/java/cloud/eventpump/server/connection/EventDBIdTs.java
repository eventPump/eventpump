package cloud.eventpump.server.connection;

class EventDBIdTs {

    long id;
    long ts;

    void set(long id, long ts) {
        this.id = id;
        this.ts = ts;
    }
}
