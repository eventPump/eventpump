package cloud.eventpump.tests.testTools;


import cloud.eventpump.server.EventsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServerFakeDB implements EventsProvider<TEvent> {

    private Runnable commit;
    private final List<String> content = new ArrayList<>();
    private List<List<TEvent>> readEvents = new ArrayList<>();

    public void setCommit(Runnable commit) {
        this.commit = commit;
    }

    public void insert(String... values) {
        for (String value : values) {
            content.add(value);
        }
        commit.run();
    }

    @Override
    public List<TEvent> provideEvents(long previouslyProcessedIncreasingEventId, int maxEventCount) {
        List<TEvent> result = new ArrayList<>();
        for (int i = 0; i < maxEventCount; i++) {
            int idx = (int) previouslyProcessedIncreasingEventId + i;
            if (idx >= content.size()) {
                break;
            }
            result.add(new TEvent(idx + 1, content.get(idx)));
        }
        readEvents.add(result);
        if (!result.isEmpty()) {
            return result;
        } else {
            return null;
        }
    }

    public String strContent() {
        return content.stream().collect(Collectors.joining("\n"));
    }

    public List<List<TEvent>> getLastReads() {
        return readEvents;
    }

    public void clearLastReads() {
        readEvents.clear();
    }
}
