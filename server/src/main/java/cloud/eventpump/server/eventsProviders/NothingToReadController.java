package cloud.eventpump.server.eventsProviders;


import cloud.eventpump.server.EventsProvider;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NothingToReadController<Event> implements EventsProvider<Event> {

    private final EventsProvider<Event> eventsProvider;
    private final Set<Long> noDataDBIds = new HashSet<>();

    public NothingToReadController(EventsProvider<Event> eventsProvider) {
        this.eventsProvider = eventsProvider;
    }

    @Override
    synchronized public List<Event> provideEvents(long dbId, int maxEventCount) {
        if (noDataDBIds.contains(dbId)) {
            return Collections.emptyList();
        }
        List<Event> events = eventsProvider.provideEvents(dbId, maxEventCount);
        if (events.isEmpty()) {
            noDataDBIds.add(dbId);
            return Collections.emptyList();
        }
        return events;
    }

    synchronized public void clear() {
        noDataDBIds.clear();
    }

}
