package cloud.eventpump.tcpClient.connection.marshaller;


import cloud.eventpump.common.FrameNo;
import cloud.eventpump.common.frame.OutputFrameBuilder;
import cloud.eventpump.common.marshaller.Marshaller;

public class InitMarshaller extends Marshaller {

    public InitMarshaller(OutputFrameBuilder outputFrameBuilder) {
        super(outputFrameBuilder);
    }

    public void init(long dbSequence, String clientName) {
        outputFrameBuilder.startFrame(FrameNo.INIT_v0100);
        outputFrameBuilder.putLong(dbSequence);
        outputFrameBuilder.putString(clientName);
        outputFrameBuilder.finishFrame();
    }

}
