package cloud.eventpump.tests.testTools;


import cloud.eventpump.server.EventAdapter;

public class TEventAdapter implements EventAdapter<TEvent> {
    @Override
    public long getEventId(TEvent tEvent) {
        return tEvent.getDbSequence();
    }

    @Override
    public long getTimestampUTC(TEvent tEvent) {
        return tEvent.getUtcTS();
    }
}
