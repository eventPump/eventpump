package cloud.eventpump.server.connection;


import cloud.eventpump.common.unexpected.CloseAppException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BufferedEventsInfoTest {

    @Test
    public void normalOperationMultiTest() {
        for (int i = 4; i < 16; i++) {
            normalOperationTest(new BufferedEventsInfo(i));
        }
    }

    @Test(expected = CloseAppException.class)
    public void eventsBufferFullTest() {
        BufferedEventsInfo cyclicBuffer = new BufferedEventsInfo(2) ;
        cyclicBuffer.put( 5,1005);
        cyclicBuffer.put( 6,1006);
        cyclicBuffer.put( 7,1007);
    }

    private void normalOperationTest(BufferedEventsInfo cyclicBuffer) {
        append(cyclicBuffer,1, 2, 3);
        checkNotFound(cyclicBuffer,0, 4, 100);
        getAndCheck(cyclicBuffer,3, 2, 1);
        append(cyclicBuffer,4, 5, 6);
        checkNotFound(cyclicBuffer,2, 7, 100);
        getAndCheck(cyclicBuffer,4, 4, 2);
        checkNotFound(cyclicBuffer,2, 3, 4, 7, 100);
        getAndCheck(cyclicBuffer,2, 5, 1);
        append(cyclicBuffer,7, 8);
        getAndCheck(cyclicBuffer,3, 8, 0);
        checkNotFound(cyclicBuffer,2, 3, 4, 7, 8, 13, 14, 100);
        append(cyclicBuffer,9, 10, 11, 12);
        getAndCheck(cyclicBuffer,4, 9, 3);
        checkNotFound(cyclicBuffer,2, 3, 4, 7, 8, 13, 100);
        getAndCheck(cyclicBuffer,3, 11, 1);
        append(cyclicBuffer,13, 14);
        getAndCheck(cyclicBuffer,3, 14, 0);
        checkNotFound(cyclicBuffer,2, 3, 4, 7, 8, 13, 14, 100);
    }

    private void checkNotFound(BufferedEventsInfo cyclicBuffer, int... values) {
        for (int i = 0; i < values.length; i++) {
            assertEquals(null, cyclicBuffer.removeUntil(values[i]));
        }
    }


    private void append(BufferedEventsInfo cyclicBuffer, int... values) {
        for (int i = 0; i < values.length; i++) {
            cyclicBuffer.put(values[i], 1000 + values[i]);
        }
    }

    private void getAndCheck(BufferedEventsInfo cyclicBuffer, int sizeBefore, int id, int sizeAfter) {
        assertEquals(sizeBefore, cyclicBuffer.getSize());
        EventDBIdTs eventDBIdTs = cyclicBuffer.removeUntil(id);
        assertEquals(sizeAfter, cyclicBuffer.getSize());
        assertEquals(id + 1000L, eventDBIdTs.ts);
    }

}