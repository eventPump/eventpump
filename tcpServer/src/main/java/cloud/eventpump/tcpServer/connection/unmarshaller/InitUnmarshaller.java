package cloud.eventpump.tcpServer.connection.unmarshaller;


import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.Unmarshaller;


public class InitUnmarshaller implements Unmarshaller {

    private final UnmarshallerProcessors epDataConnectionProvides;

    public InitUnmarshaller(UnmarshallerProcessors epDataConnectionProvides) {
        this.epDataConnectionProvides = epDataConnectionProvides;
    }

    @Override
    public void unmarshall(PrimitivesReadBuffer primitivesReadBuffer) {
        epDataConnectionProvides.startDBId(primitivesReadBuffer.getLong());
        epDataConnectionProvides.clientName(primitivesReadBuffer.getString());
    }


}
