package cloud.eventpump.tcpServer.connection.unmarshaller;


import cloud.eventpump.common.buffers.PrimitivesReadBuffer;
import cloud.eventpump.common.marshaller.Unmarshaller;

public class AckUnmarshaller implements Unmarshaller {

    private final UnmarshallerProcessors epDataConnectionProvides;
    private long ack = Long.MIN_VALUE;

    public AckUnmarshaller(UnmarshallerProcessors epDataConnectionProvides) {
        this.epDataConnectionProvides = epDataConnectionProvides;
    }

    @Override
    public void unmarshall(PrimitivesReadBuffer primitivesReadBuffer) {
        ack = primitivesReadBuffer.getLong();
    }

    public void flush() {
        if (ack != Long.MIN_VALUE) {
            epDataConnectionProvides.ack(ack);
            ack = Long.MIN_VALUE;
        }
    }

}
