package cloud.eventpump.statisticsServer.connection;

import cloud.eventpump.common.UpdateConnectionStatistics;
import cloud.eventpump.statisticsServer.ConnectionStatistics;


public class ConnectionStatisticCalculator implements UpdateConnectionStatistics, ConnectionStatistics {

    private String clientName = "UNKNOWN";

    private long lastReceivedAckDbId = -1;
    private long lastReceivedAckUtcTs = -1;
    private long lastReceivedAckEventUtcTs = 0;
    private boolean clientSynchronized = false;

    private int totalSentBytesCount = 0;
    private int totalReceivedBytesCount = 0;

    private int totalReadEventsCount = 0;
    private int eventsRemainigInBuffer = 0;

    @Override
    public void applyAck(long ackId, long ackTs, int bufferRemaining) {
        this.lastReceivedAckDbId = ackId;
        this.lastReceivedAckUtcTs = System.currentTimeMillis();
        this.lastReceivedAckEventUtcTs = ackTs;
        this.eventsRemainigInBuffer = bufferRemaining;
    }

    @Override
    public void applyReadEventsCount(int readEventsCount) {
        this.totalReadEventsCount += readEventsCount;
    }

    @Override
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @Override
    public void applySentBytesCount(int sentBytesCount) {
        totalSentBytesCount += sentBytesCount;
    }

    @Override
    public void applyReceivedBytesCount(int receivedBytesCount) {
        totalReceivedBytesCount += receivedBytesCount;
    }

    @Override
    public void setClientSynchronized(boolean clientSynchronized) {
        this.clientSynchronized = clientSynchronized;
    }

    // Connection stqatistics
    @Override
    public String getClientName() {
        return clientName;
    }

    @Override
    public long getLastReceivedAckDbId() {
        return lastReceivedAckDbId;
    }

    @Override
    public long getLastReceivedAckUtcTs() {
        return lastReceivedAckUtcTs;
    }

    @Override
    public long getLastReceivedAckEventUtcTs() {
        return lastReceivedAckEventUtcTs;
    }

    @Override
    public long getClientLag() {
        if (lastReceivedAckEventUtcTs != -1) {
            return lastReceivedAckUtcTs - lastReceivedAckEventUtcTs;
        } else {
            return -1;
        }
    }

    @Override
    public boolean isClientSynchronized() {
        return clientSynchronized;
    }

    @Override
    public int getTotalSentBytesCount() {
        return totalSentBytesCount;
    }

    @Override
    public int getTotalReceivedBytesCount() {
        return totalReceivedBytesCount;
    }

    @Override
    public int getTotalReadEventsCount() {
        return totalReadEventsCount;
    }

    @Override
    public int getTotalAckEventsCount() {
        return totalReadEventsCount - eventsRemainigInBuffer;
    }
}
