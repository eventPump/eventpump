package cloud.eventpump.tests.cache;

import cloud.eventpump.server.cache.EventCache;
import cloud.eventpump.server.cache.EventCacheStorage;
import cloud.eventpump.server.connection.ReadController;
import cloud.eventpump.server.eventsProviders.NothingToReadController;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class CacheTest {

    private EventCacheStorage<Integer> eventCacheStorage;
    private EventCache<Integer> eventCache;
    private NothingToReadController<Integer> nothingToReadController;
    private ReadController<Integer> rc1;
    private ReadController<Integer> rc2;
    private List<Integer> values;

    @Before
    public void before() {
        eventCacheStorage = new EventCacheStorage<>();
        eventCache = new EventCache<>(this::provide, event -> event, eventCacheStorage);
        nothingToReadController = new NothingToReadController<>(eventCache);
        rc1 = new ReadController<>(nothingToReadController, 0);
        rc2 = new ReadController<>(nothingToReadController, 0);
        values = new ArrayList<>();
    }

    @Test
    public void readControllerCacheCooperationTest() {
        commit(1);
        checkListContainsOneElement(1, rc1.provideEvents(0, 2));
        commit(2);
        checkListContainsOneElement(2, rc1.provideEvents(1, 2));
        commit(3);
        checkListContainsOneElement(3, rc1.provideEvents(2, 2));

        List<Integer> r2 = rc2.provideEvents(0, 2);
        assertEquals(2, r2.size());
        assertEquals(1, (int) r2.get(0));
        assertEquals(2, (int) r2.get(1));

        r2 = rc2.provideEvents(2, 2);
        assertEquals(1, r2.size());
        assertEquals(3, (int) r2.get(0));
    }

    private void checkListContainsOneElement(int expected, List<Integer> actual) {
        assertEquals(1, actual.size());
        assertEquals(expected, (int) actual.get(0));
    }

    private void commit(int value) {
        values.add(value);
        nothingToReadController.clear();
        rc1.clear();
        rc2.clear();
    }

    private List<Integer> provide(long dbId, int maxCount) {
        List<Integer> result = new ArrayList<>();
        for (int i = (int) dbId; i < values.size(); i++) {
            result.add(values.get(i));
        }
        return result;
    }


}
