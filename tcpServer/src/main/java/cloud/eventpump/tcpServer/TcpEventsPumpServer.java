package cloud.eventpump.tcpServer;


import cloud.eventpump.server.EPSClientHandle;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.tcpNIOServer.TcpNIOServer;
import cloud.eventpump.tcpServer.connection.ConnectionManager;

import java.io.IOException;

public class TcpEventsPumpServer<Event> {

    private final EPSClientHandle<Event> epClient;
    private final TcpServerConfig<Event> config;

    private TcpNIOServer server;

    public TcpEventsPumpServer(EventsPumpServer<Event> eventsPumpServer, TcpServerConfig<Event> config) {
        this.epClient = eventsPumpServer.getEPSClientHandle();
        this.config = config;
    }

    public void start() throws IOException {
        ConnectionManager connectionManager = new ConnectionManager<>(
                config.getEventMarshaller(),
                epClient,
                config.getWriteBufferSize(),
                config.getUnexpectedHandler());
        server = new TcpNIOServer(connectionManager, config.getUnexpectedHandler());
        epClient.appendCommitEventConsumer(server::sendInterrupt);
        server.start(config.getServerAddress());
    }

    public void close() {
        server.close();
        // TODO unregister form EPS ??
    }

}
