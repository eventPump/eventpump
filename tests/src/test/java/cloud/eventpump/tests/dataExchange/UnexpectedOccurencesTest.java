package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.common.EventsApplier;
import cloud.eventpump.common.buffers.WriteMultiBuffer;
import cloud.eventpump.common.frame.OutputFrameBuilder;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.ServerStatistics;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpClient.connection.ConnectionProcessor;
import cloud.eventpump.tcpNIOServer.TcpNIOClient;
import cloud.eventpump.tcpNIOServer.connection.TcpNIOConnectionProvides;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UnexpectedOccurencesTest {

    @Test
    public void badMessageTest() throws IOException, InterruptedException {
        EventsPumpServer<TEvent> eventEventsPumpServer = new EventsPumpServer<>(
                (dbId, maxEventCount) -> Arrays.asList(new TEvent(1, "a")),
                TEvent::getDbSequence, new ServerConfig());
        StatisticsEventPumpServer<TEvent> statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>( new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress( new InetSocketAddress(5671));
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        TcpClientConfig<TEvent> c1Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c1Configuration.setClientName("c1");
        c1Configuration.setServer( new InetSocketAddress(5671));

        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return 1;
            }
        },c1Configuration);
        c1.start();

        BadMessageSendingTcpEventsPumpClient<TEvent> c2 = new BadMessageSendingTcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return 1;
            }
        });
        c2.start(0, TEvent.class);
        Thread.sleep(100);

        ServerStatistics statistics = statisticsEventPumpServer.getStatistics();
        assertEquals(2, statistics.getConnectionStatistics().size());

        c2.sendFrameWithUnrecognizedFrameNo();
        Thread.sleep(100);
        assertEquals(1, statistics.getConnectionStatistics().size());

        TcpClientConfig<TEvent> c3Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c3Configuration.setClientName("c3");
        c3Configuration.setServer(new InetSocketAddress(5671));
        TcpEventsPumpClient<TEvent> c3 = new TcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return 1;
            }
        }, c3Configuration);
        c3.start();

        Thread.sleep(100);
        assertEquals(2, statistics.getConnectionStatistics().size());

        c1.close();
        c3.close();
        tcpEventsPumpServer.close();
    }

    @Test
    public void clientCloseConnectionTest() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        serverFakeDB.setCommit(eventEventsPumpServer::commit);
        StatisticsEventPumpServer<TEvent> statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>(new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress( new InetSocketAddress(5671));
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        TcpClientConfig<TEvent> c1Configuration = TcpClientConfig.create( new JacksonUnmarshaller<>(TEvent.class));
        c1Configuration.setClientName("c1");
        c1Configuration.setServer(new InetSocketAddress(5671));
        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return tEvents.get( tEvents.size()-1).getDbSequence();
            }
        }, c1Configuration);
        c1.start();

        TcpClientConfig<TEvent> c2Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c2Configuration.setClientName("c2");
        c2Configuration.setServer(new InetSocketAddress(5671));
        TcpEventsPumpClient<TEvent> c2 = new TcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return tEvents.get( tEvents.size()-1).getDbSequence();
            }
        },c2Configuration);
        c2.start();
        Thread.sleep(100);

        ServerStatistics statistics = statisticsEventPumpServer.getStatistics();
        assertEquals(2, statistics.getConnectionStatistics().size());

        c2.close();
        Thread.sleep(10);
        serverFakeDB.insert("a");

        Thread.sleep(10);
        assertEquals(1, statistics.getConnectionStatistics().size());

        c1.close();
        tcpEventsPumpServer.close();
    }

    @Test
    public void badAckTest() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        serverFakeDB.setCommit(eventEventsPumpServer::commit);
        StatisticsEventPumpServer<TEvent> statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>(new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress( new InetSocketAddress(5671));
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        TcpClientConfig<TEvent> c1Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c1Configuration.setClientName("c1");
        c1Configuration.setServer(new InetSocketAddress(5671));
        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return tEvents.get( tEvents.size()-1).getDbSequence();
            }
        }, c1Configuration);
        c1.start();

        // It's only warning
        TcpClientConfig<TEvent> c2Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c2Configuration.setClientName("c2");
        c2Configuration.setServer(new InetSocketAddress(5671));
        TcpEventsPumpClient<TEvent> c2 = new TcpEventsPumpClient<>(new EventsApplier<TEvent>() {
            @Override
            public long findLastAppliedDBId() {
                return 0;
            }

            @Override
            public long applyEvents(List<TEvent> tEvents) {
                return 337;
            }
        },c2Configuration);
        c2.start();

        Thread.sleep(10);

        serverFakeDB.insert("a");

        Thread.sleep(10);

        ServerStatistics statistics = statisticsEventPumpServer.getStatistics();
        assertEquals(1, statistics.getConnectionStatistics().size());

        Thread.sleep(10);

        c1.close();
        tcpEventsPumpServer.close();
    }
    
    public static class BadMessageSendingTcpEventsPumpClient<Event> {

        private final EventsApplier<Event> eventsApplier;
        private TcpNIOClient client;
        private ConnectionProcessor<Event> connectionProcessor;
        private TcpNIOConnectionProvides tcpNIOConnectionProvides;

        public BadMessageSendingTcpEventsPumpClient(EventsApplier<Event> eventsApplier) {
            this.eventsApplier = eventsApplier;
        }

        public void start(long lastAppliedDBId, Class<Event> eventClass) throws IOException {
            client = new TcpNIOClient();
            connectionProcessor = new ConnectionProcessor<>(eventsApplier, new JacksonUnmarshaller<>(eventClass), new UnexpectedHandler(),10,10000);
            tcpNIOConnectionProvides = client.init(new InetSocketAddress("localhost", 5671), null, connectionProcessor);
            connectionProcessor.setTcpNIOConnectionProvides(tcpNIOConnectionProvides);
            Thread thread = new Thread( () -> {
                try {
                    client.mainLoop(0) ;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } ) ;
            thread.start();
            connectionProcessor.init(lastAppliedDBId, "Bad message sending Client");
        }

        public void sendFrameWithUnrecognizedFrameNo() {
            WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(100);
            OutputFrameBuilder outputFrameBuilder = new OutputFrameBuilder(writeMultiBuffer);
            outputFrameBuilder.startFrame(787878);
            outputFrameBuilder.finishFrame();
            tcpNIOConnectionProvides.send(writeMultiBuffer.getByteBuffers());
        }

        public void ack(long ack) {
            connectionProcessor.ack(ack);
        }

        public void close() {
            client.close();
        }
    }

}
