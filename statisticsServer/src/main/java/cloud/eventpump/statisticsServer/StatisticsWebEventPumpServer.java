package cloud.eventpump.statisticsServer;

import cloud.eventpump.statisticsServer.html.StatisticsJsonBuilder;
import spark.Spark;


public class StatisticsWebEventPumpServer<Event> {

    private final StatisticsEventPumpServer<Event> statisticsEventPumpServer;
    private final StatisticsJsonBuilder statisticsJsonBuilder = new StatisticsJsonBuilder();

    private final StatisticsWebConfig statisticsWebConfig ;

    public StatisticsWebEventPumpServer(StatisticsEventPumpServer<Event> statisticsEventPumpServer, StatisticsWebConfig statisticsWebConfig) {
        this.statisticsEventPumpServer = statisticsEventPumpServer;
        this.statisticsWebConfig = statisticsWebConfig ;
    }

    public void start() {
        Spark.port(statisticsWebConfig.getPort());
        Spark.threadPool(statisticsWebConfig.getMaxHttpServerThreadCount());
        Spark.staticFiles.location("/html");
        configureRestEndpoint();
    }

    public void close() {
        Spark.stop();
    }

    private void configureRestEndpoint() {
        ServerStatistics statistics = statisticsEventPumpServer.getStatistics();
        Spark.get(statisticsWebConfig.getRestPath(), (req, res) -> {
            res.type("application/json");
            return statisticsJsonBuilder.build(statistics);
        });
    }
}
