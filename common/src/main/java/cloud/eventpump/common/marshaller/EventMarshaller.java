package cloud.eventpump.common.marshaller;

import cloud.eventpump.common.buffers.PrimitivesWriteBuffer;

public interface EventMarshaller<Event> {
    void marshall(Event event, PrimitivesWriteBuffer writeBuffer) throws Exception;
}
