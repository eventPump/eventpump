package cloud.eventpump.utils.marshaller;

import cloud.eventpump.common.buffers.PrimitivesWriteBuffer;
import cloud.eventpump.common.marshaller.EventMarshaller;
import cloud.eventpump.common.unexpected.CloseAppException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonMarshaller<Event> implements EventMarshaller<Event> {

    private final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Override
    public void marshall(Event event, PrimitivesWriteBuffer writeBuffer) {
        try {
            writeBuffer.putString(mapper.writeValueAsString(event));
        } catch (JsonProcessingException ex) {
            throw new CloseAppException(ex);
        }
    }
}
