package cloud.eventpump.common.buffers;

public class PrimitiveConversions {

    // TODO different bytes order

    public static long bytesToLong(byte[] longBytesArray) {
        return (0xffL & longBytesArray[0]) |
                (0xffL & longBytesArray[1]) << 8 |
                (0xffL & longBytesArray[2]) << 16 |
                (0xffL & longBytesArray[3]) << 24 |
                (0xffL & longBytesArray[4]) << 32 |
                (0xffL & longBytesArray[5]) << 40 |
                (0xffL & longBytesArray[6]) << 48 |
                (0xffL & longBytesArray[7]) << 56;
    }

    public static int bytesToInt(byte[] bytesIntArray) {
        return (0xff & bytesIntArray[0]) |
                (0xff & bytesIntArray[1]) << 8 |
                (0xff & bytesIntArray[2]) << 16 |
                (0xff & bytesIntArray[3]) << 24;
    }

    public static byte[] intToBytes(int value) {
        return new byte[]{
                (byte) (value & 0xFF),
                (byte) ((value >> 8) & 0xFF),
                (byte) ((value >> 16) & 0xFF),
                (byte) ((value >> 24) & 0xFF)
        };
    }

    public static byte[] longToBytes(long value) {
        return new byte[]{
                (byte) (value & 0xFF),
                (byte) ((value >> 8) & 0xFF),
                (byte) ((value >> 16) & 0xFF),
                (byte) ((value >> 24) & 0xFF),
                (byte) ((value >> 32) & 0xFF),
                (byte) ((value >> 40) & 0xFF),
                (byte) ((value >> 48) & 0xFF),
                (byte) ((value >> 56) & 0xFF)
        };
    }

}
