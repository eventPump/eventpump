package cloud.eventpump.common;


public interface UpdateServerStatistics extends EventCacheStatistics {

    default UpdateConnectionStatistics createConnection() {
        return new UpdateConnectionStatistics() {};
    }

    default void closeConnection(UpdateConnectionStatistics updateConnectionStatistics) {}

    default void setServerName(String s) {}
}
