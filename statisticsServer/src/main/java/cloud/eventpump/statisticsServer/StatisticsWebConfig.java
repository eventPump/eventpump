package cloud.eventpump.statisticsServer;

public class StatisticsWebConfig {

    private int port = 9898;
    private int maxHttpServerThreadCount = 10;
    private String restPath = "/v1/serverStatistics";

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getMaxHttpServerThreadCount() {
        return maxHttpServerThreadCount;
    }

    public void setMaxHttpServerThreadCount(int maxHttpServerThreadCount) {
        this.maxHttpServerThreadCount = maxHttpServerThreadCount;
    }

    public String getRestPath() {
        return restPath;
    }

    public void setRestPath(String restPath) {
        this.restPath = restPath;
    }
}
