package cloud.eventpump.server;


import cloud.eventpump.common.UpdateConnectionStatistics;
import cloud.eventpump.common.UpdateServerStatistics;
import cloud.eventpump.common.unexpected.UnexpectedHandler;
import cloud.eventpump.server.connection.EPSConnectionManager;
import cloud.eventpump.server.connection.EPSConnectionProvides;
import cloud.eventpump.server.connection.EPSConnectionRequires;

import java.util.ArrayList;
import java.util.List;

class EPSClientHandler<Event> implements EPSClientHandle<Event> {

    private final EPSConnectionManager<Event> epsConnectionManager;
    private final List<Runnable> commitClients = new ArrayList<>();
    private final int maxBufferedEventsCount ;
    private final int eventsReadThreshold ;
    private UpdateServerStatistics updateServerStatistics = new UpdateServerStatistics() {
    };

    EPSClientHandler(EventsProvider<Event> eventsProvider, EventAdapter<Event> eventAdapter,
                     int maxBufferedEventsCount, int eventsReadThreshold, UnexpectedHandler unexpectedHandler) {
        this.epsConnectionManager = new EPSConnectionManager<>(eventsProvider, eventAdapter, unexpectedHandler);
        this.maxBufferedEventsCount = maxBufferedEventsCount ;
        this.eventsReadThreshold = eventsReadThreshold ;
    }

    void setStatisticsServer(UpdateServerStatistics updateServerStatistics) {
        this.updateServerStatistics = updateServerStatistics;
    }

    void commit() {
        commitClients.forEach(Runnable::run);
    }

    @Override
    public EPSConnectionProvides createConnection(EPSConnectionRequires<Event> eventEPDataConnectionRequires) {
        UpdateConnectionStatistics updateConnectionStatistics = updateServerStatistics.createConnection();
        EPSConnectionProvides epDataConnectionProvides = epsConnectionManager.createConnection(
                eventEPDataConnectionRequires, updateConnectionStatistics,
                eventsReadThreshold, maxBufferedEventsCount);
        return epDataConnectionProvides;
    }

    @Override
    public void closeConnection(EPSConnectionProvides epsConnectionProvides) {
        UpdateConnectionStatistics updateConnectionStatistics = epsConnectionManager.closeConnection(epsConnectionProvides);
        if (updateConnectionStatistics != null) {
            updateServerStatistics.closeConnection(updateConnectionStatistics);
        }
    }


    @Override
    public void appendCommitEventConsumer(Runnable commitEventConsumer) {
        commitClients.add(commitEventConsumer);
    }

}
