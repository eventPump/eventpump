package cloud.eventpump.common.frame;

import cloud.eventpump.common.buffers.ReadMultiBuffer;
import cloud.eventpump.common.buffers.WriteMultiBuffer;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InputFrameCollectorTest {

    @Test
    public void inputFrameCollectorTest() {
        ReadMultiBuffer readMultiBuffer = new ReadMultiBuffer();
        InputFrameCollector inputFrameCollector = new InputFrameCollector(readMultiBuffer);

        WriteMultiBuffer writeMultiBuffer = new WriteMultiBuffer(3);
        OutputFrameBuilder outputFrameBuilder = new OutputFrameBuilder(writeMultiBuffer);
        outputFrameBuilder.startFrame(13);
        outputFrameBuilder.putString("abc");
        outputFrameBuilder.finishFrame();
        outputFrameBuilder.startFrame(14);
        outputFrameBuilder.putString("frame");
        outputFrameBuilder.finishFrame();

        assertEquals(-1, inputFrameCollector.getNextFrameNo());
        List<ByteBuffer> byteBuffers = writeMultiBuffer.getByteBuffers();
        byteBuffers.forEach(readMultiBuffer::append);
        assertEquals(13, inputFrameCollector.getNextFrameNo());
        assertEquals("abc", inputFrameCollector.getString());
        inputFrameCollector.markFrameFinished();
        assertEquals(14, inputFrameCollector.getNextFrameNo());
        assertEquals("frame", inputFrameCollector.getString());
        inputFrameCollector.markFrameFinished();
        assertEquals(-1, inputFrameCollector.getNextFrameNo());
    }


}