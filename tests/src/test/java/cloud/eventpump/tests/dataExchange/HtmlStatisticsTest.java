package cloud.eventpump.tests.dataExchange;

import cloud.eventpump.server.EventsPumpServer;
import cloud.eventpump.server.ServerConfig;
import cloud.eventpump.statisticsServer.StatisticsEventPumpServer;
import cloud.eventpump.statisticsServer.StatisticsWebConfig;
import cloud.eventpump.statisticsServer.StatisticsWebEventPumpServer;
import cloud.eventpump.tcpClient.TcpClientConfig;
import cloud.eventpump.tcpClient.TcpEventsPumpClient;
import cloud.eventpump.tcpServer.TcpEventsPumpServer;
import cloud.eventpump.tcpServer.TcpServerConfig;
import cloud.eventpump.tests.testTools.ClientFakeDB;
import cloud.eventpump.tests.testTools.ServerFakeDB;
import cloud.eventpump.tests.testTools.TEvent;
import cloud.eventpump.tests.testTools.TEventAdapter;
import cloud.eventpump.utils.marshaller.JacksonMarshaller;
import cloud.eventpump.utils.marshaller.JacksonUnmarshaller;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;

public class HtmlStatisticsTest {

    @Test
    public void htmlStatiscicsTest() throws IOException, InterruptedException {
        ServerFakeDB serverFakeDB = new ServerFakeDB();
        EventsPumpServer<TEvent> eventEventsPumpServer = new EventsPumpServer<>(serverFakeDB, new TEventAdapter(), new ServerConfig());
        serverFakeDB.setCommit(eventEventsPumpServer::commit);
        StatisticsEventPumpServer<TEvent> statisticsEventPumpServer = new StatisticsEventPumpServer<>(eventEventsPumpServer);
        StatisticsWebEventPumpServer<TEvent> statisticsWebEventPumpServer = new StatisticsWebEventPumpServer<>(statisticsEventPumpServer, new StatisticsWebConfig());
        statisticsWebEventPumpServer.start();

        TcpServerConfig<TEvent> tcpServerConfig = new TcpServerConfig<>(new JacksonMarshaller<>());
        tcpServerConfig.setServerAddress(new InetSocketAddress(5671));
        TcpEventsPumpServer<TEvent> tcpEventsPumpServer = new TcpEventsPumpServer<>(eventEventsPumpServer, tcpServerConfig);
        tcpEventsPumpServer.start();

        TcpClientConfig<TEvent> c1Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c1Configuration.setClientName("c1");
        c1Configuration.setServer(new InetSocketAddress(5671));
        ClientFakeDB clientFakeDB = new ClientFakeDB();
        clientFakeDB.setAutoAck(true);
        TcpEventsPumpClient<TEvent> c1 = new TcpEventsPumpClient<>(clientFakeDB, c1Configuration);
        c1.start();

        TcpClientConfig<TEvent> c2Configuration = TcpClientConfig.create(new JacksonUnmarshaller<>(TEvent.class));
        c2Configuration.setClientName("c2");
        c2Configuration.setServer(new InetSocketAddress(5671));
        ClientFakeDB clientFakeDB2 = new ClientFakeDB();
        clientFakeDB2.setAutoAck(true);
        TcpEventsPumpClient<TEvent> c2 = new TcpEventsPumpClient<>(clientFakeDB2, c2Configuration);

//        for ( int j = 0 ; j < 100 ; j++ ) {
//            c2.start();
//            for (int i = 0; i < 500; i++) {
//                serverFakeDB.insert("a" + i);
//                Thread.sleep(60);
//            }
//            c2.close();
//            for (int i = 0; i < 500; i++) {
//                serverFakeDB.insert("a" + i);
//                Thread.sleep(60);
//            }
//        }
//        Thread.sleep( 60000);

        c1.close();
        c2.close();
        tcpEventsPumpServer.close();
        statisticsWebEventPumpServer.close();

    }


}
